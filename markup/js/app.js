$(function () {
    $('a.js-animate-scroll').click(function () {
        var _href = $(this).attr('href');
        $('html, body').animate({
            scrollTop: ($(_href).offset().top - 101) + 'px'
        });
        return false;
    });

    $('.js-header-search-btn').click(function () {
        var $header = $('.js-header');

        if ($header.hasClass('header--searchable')) {
            $header.find('form').submit();
        } else {
            $header.addClass('header--searchable');
        }
    });

    $('.js-header-search-back').click(function () {
        $('.js-header').removeClass('header--searchable');
    });

    $('.js-header-burger').click(function () {
        $('.js-left-menu').toggle();
        $('.js-left-menu-bg').toggle();
    });

    $('.js-left-menu-close, .js-left-menu-bg').click(function () {
        $('.js-left-menu').hide();
        $('.js-left-menu-bg').hide();
    })

    // Right content scrolling
    var rightContentMt = 0;
    var prevWindowScrollTop = 0;
    $(window).scroll(function () {
        var marginFromTop = 150;
        var $rightContent = $('.js-right-content');
        var $rightContentInner = $rightContent.find('.js-right-content-inner');
        var windowHeight = $(window).height();
        var windowScrollTop = $(window).scrollTop();
        var rightContentInnerHeight = $rightContentInner.height();

        // console.log('windowScrollTop ' + windowScrollTop);
        // console.log('windowHeight ' + windowHeight);
        // console.log('rightContentInnerHeight ' + rightContentInnerHeight);
        // console.log('rightContentMt ' + rightContentMt);

        if (windowScrollTop < prevWindowScrollTop) {

            if (windowScrollTop < rightContentInnerHeight - windowHeight + marginFromTop) {
                $rightContent.removeClass('right-content-fixed right-content-fixed--b0');
            }

            if (!$rightContent.hasClass('right-content--mt')) {
                if (windowScrollTop > rightContentInnerHeight) {
                    rightContentMt = windowScrollTop - rightContentInnerHeight + windowHeight - marginFromTop;

                    $rightContent.css('margin-top', rightContentMt);
                    $rightContent.removeClass('right-content-fixed right-content-fixed--h100 right-content-fixed--b0').addClass('right-content--mt');
                }
            } else {
                if (windowScrollTop < rightContentMt) {
                    rightContentMt = windowScrollTop;
                    $rightContent.addClass('right-content-fixed right-content-fixed--t0').css('margin-top', 70);
                }
            }

        } else {
            if ($rightContent.hasClass('right-content-fixed--t0')) {
                $rightContent.css('margin-top', rightContentMt);
            }

            $rightContent.removeClass('right-content-fixed right-content-fixed--t0');

            if (!$rightContent.hasClass('right-content--mt') || windowScrollTop > rightContentMt + rightContentInnerHeight - windowHeight + marginFromTop) {

                if ($rightContent.hasClass('right-content--mt')) {
                    $rightContent.removeClass('right-content--mt').css('margin-top', 0);
                }

                if (windowHeight - marginFromTop > rightContentInnerHeight) {
                    $rightContent.addClass('right-content-fixed right-content-fixed--h100');
                } else {
                    if (windowScrollTop > rightContentInnerHeight - windowHeight + marginFromTop) {
                        $rightContent.addClass('right-content-fixed right-content-fixed--b0');
                    } else {
                        $rightContent.removeClass('right-content-fixed right-content-fixed--b0');
                    }
                }
            }
        }

        prevWindowScrollTop = $(window).scrollTop();
    });

    // Arrow to top
    $(window).scroll(function () {
        if ($(this).scrollTop() > $(window).height() + 200) {
            $('.js-scroll-to-top').fadeIn();
        } else {
            $('.js-scroll-to-top').fadeOut();
        }
    });

    $('.js-sociomics-detail-other-slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        prevArrow: '<div class="sociomics-detail-content__bottom__other-items-prev"><img src="/resources/img/sociomics-detail/slider-arrow-left.svg"></div>',
        nextArrow: '<div class="sociomics-detail-content__bottom__other-items-next"><img src="/resources/img/sociomics-detail/slider-arrow-right.svg"></div>',
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    $('.js-release-editing-menu-button').click(function () {
        $(this).parent().find('.js-release-editing-menu').toggle();
    });

    $(document).mouseup(function (e) {
        var releaseEditingMenuButton = $('.js-release-editing-menu-button');
        var releaseEditingMenu = releaseEditingMenuButton.parent().find('.js-release-editing-menu');

        if (!releaseEditingMenuButton.is(e.target) && releaseEditingMenuButton.has(e.target).length === 0) {
            releaseEditingMenu.hide();
        }
    });

    $('.js-sociomics-editing-menu-button').click(function () {
        $(this).parent().find('.js-sociomics-editing-menu').toggle();
    });

    $(document).mouseup(function (e) {
        var sociomicsEditingMenuButton = $('.js-sociomics-editing-menu-button');
        var sociomicsEditingMenu = sociomicsEditingMenuButton.parent().find('.js-sociomics-editing-menu');

        if (!sociomicsEditingMenuButton.is(e.target) && sociomicsEditingMenuButton.has(e.target).length === 0) {
            sociomicsEditingMenu.hide();
        }
    });

    // Открытие модалок по клику на ссылку
    $('.modal-link').magnificPopup({
        closeOnBgClick: true,
        showCloseBtn: false
    });

    // Закрытие модалок
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    // Соглашение на использование Cookie
    if ($.cookie('hideCookieModal') !== 'true') {
        setTimeout(function () {
            $('.js-cookie-modal').fadeIn();
        }, 1000);

        $('.js-close-cookie-modal').click(function () {
            $('.js-cookie-modal').fadeOut();
        });

        $.cookie('hideCookieModal', true, {
            expires: 1,
            path: '/'
        });
    }

    setInterval(function () {
        $('.g-recaptcha-response').attr('required', 'required');
    }, 3000);
});