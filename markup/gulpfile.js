'use strict';

let gulp = require('gulp'),
    del = require('del'),
    browserSync = require('browser-sync').create(),
    uglifyEs = require('gulp-uglify-es').default,
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    cache = require('gulp-cached'),
    pug = require('gulp-pug');

gulp.task('pug', function buildHTML() {
    return gulp.src('pug/*.pug')
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest('build'))
});

gulp.task('sass', function () {
    return gulp
        .src([
            'scss/*.scss',
        ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest('../app/web/resources/css'))
        .pipe(browserSync.stream());
});

gulp.task('js-min', function () {
    return gulp
        .src('js/*.js')
        .pipe(cache('js-min'))
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(concat('app.js'))
        .pipe(uglifyEs())
        .on('error', console.log)
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest('../app/web/resources/js'))
        .pipe(browserSync.stream());
});

gulp.task('img', function buildHTML() {
    return gulp.src('img/*/*')
        .pipe(gulp.dest('../app/web/resources/img'))
});

gulp.task('watch', function () {
    gulp.watch('pug/*', gulp.parallel('pug'));
    gulp.watch('pug/*/*', gulp.parallel('pug'));
    gulp.watch('scss/*', gulp.parallel('sass'));
    gulp.watch('scss/*/*', gulp.parallel('sass'));
    gulp.watch('js/*', gulp.parallel('js-min'));
    gulp.watch('js/*/*', gulp.parallel('js-min'));
    gulp.watch('img/*', gulp.parallel('img'));
    gulp.watch('img/*/*', gulp.parallel('img'));
});

gulp.task('sync', function (cb) {
    browserSync.init({
        open: false,
        port: 3030,
        notify: false,
        ghostMode: {
            scroll: false
        },
        proxy: 'sociomics.markup'
    });

    cb();
});

gulp.task('default', gulp.parallel('sync', 'pug', 'sass', 'js-min', 'img', 'watch'));
