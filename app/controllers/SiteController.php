<?php

namespace app\controllers;

use Yii;
use app\models\data\Release;
use app\models\data\Sociomics;
use app\models\data\User;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{

    public function beforeAction($action)
    {
        if ($this->action->id === 'social-auth') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $releases = Release::find()
            ->where([
                'is_published' => 1,
                'language' => Yii::$app->language
            ])
            ->orderBy(['date' => SORT_DESC])
            ->all();

        $librarySociomicses = Sociomics::find()
            ->where([
                'is_published' => 1,
                'in_library' => 1,
                'language' => Yii::$app->language
            ])
            ->orderBy(['published_at' => SORT_DESC])
            ->limit(10)
            ->all();

        return $this->render('index', compact('releases', 'librarySociomicses'));
    }

    public function actionLogin()
    {
        if (Yii::$app->request->isPost) {
            $email = Yii::$app->request->post('email');
            $password = Yii::$app->request->post('password');

            $user = User::findOne(['email' => $email]);

            if ($user === null) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь с указанным e-mail не найден.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            if ($user->status === User::STATUS_REGISTERED) {
                $user->sendConfirmEmail();
                Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь с указанным e-mail зарегистрирован, но не подтвержден. На указанный e-mail отправлено письмо для подтверждения регистрации.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            if (!Yii::$app->security->validatePassword($password, $user->password_hash)) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Пароль указан неверно.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            if ($user->status === User::STATUS_ACTIVE) {
                Yii::$app->user->login($user);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSocialAuth()
    {
        if (Yii::$app->request->isPost) {
            $token = Yii::$app->request->post('token');
            $socialData = json_decode(file_get_contents('http://ulogin.ru/token.php?token=' . $token . '&host=' . $_SERVER['HTTP_HOST']), true);

            $email = $socialData['email'];
            $name = trim(($socialData['first_name'] ?? '') . ' ' . ($socialData['last_name'] ?? ''));
            $avatar = $socialData['photo'] ?? '';

            $user = User::findOne(['email' => $email]);

            if ($user !== null) {
                if ($user->status === User::STATUS_REGISTERED) {
                    $user->sendConfirmEmail();
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь с таким e-mail уже был зарегистрирован, но не подтвержден. На указанный e-mail отправлено письмо для подтверждения регистрации.'));
                } elseif ($user->status === User::STATUS_ACTIVE) {
                    Yii::$app->user->login($user);
                }

                return $this->redirect(Yii::$app->request->referrer);
            }

            $password = Yii::$app->security->generateRandomString(8);

            $user = new User([
                'email' => $email,
                'name' => !empty($name) ? $name : 'Unknown',
                'password_hash' => Yii::$app->security->generatePasswordHash($password),
                'status' => User::STATUS_ACTIVE,
                'language' => Yii::$app->language,
                'auth_key' => Yii::$app->security->generateRandomString()
            ]);

            if (!$user->save()) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось зарегистрироваться. Попробуйте еще раз или обратитесь в службу поддержки.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            if ($avatar) {
                $avatarPathInfo = pathinfo($avatar);
                $avatarExtension = substr($avatarPathInfo['extension'], 0, strpos($avatarPathInfo['extension'], '?'));
                $avatarName = time() . '.' . $avatarExtension;
                $avatarPath = '/user/' . $user->id . '/avatar/';
                $avatarFile = file_get_contents($avatar);

                if (FileHelper::createDirectory(Yii::getAlias('@webroot/uploads') . $avatarPath)
                    && @file_put_contents(Yii::getAlias('@webroot/uploads') . $avatarPath . $avatarName, $avatarFile)) {
                    $user->avatar = $avatarPath . $avatarName;
                    $user->save();
                }
            }

            Yii::$app->user->login($user);
        }

        return $this->redirect('/');
    }

    public function actionRegister()
    {
        if (Yii::$app->request->isPost) {
            $email = Yii::$app->request->post('email');
            $password = Yii::$app->request->post('password');

            $user = User::findOne(['email' => $email]);

            if ($user !== null) {
                if ($user->status === User::STATUS_REGISTERED) {
                    $user->sendConfirmEmail();
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь с таким e-mail уже был зарегистрирован, но не подтвержден. На указанный e-mail отправлено письмо для подтверждения регистрации.'));
                } else {
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь с таким e-mail уже был зарегистрирован.'));
                }

                return $this->redirect(Yii::$app->request->referrer);
            }

            $user = new User([
                'email' => $email,
                'password_hash' => Yii::$app->security->generatePasswordHash($password),
                'status' => User::STATUS_REGISTERED,
                'language' => Yii::$app->language,
                'auth_key' => Yii::$app->security->generateRandomString()
            ]);

            if (!$user->save()) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось зарегистрироваться. Попробуйте еще раз или обратитесь в службу поддержки.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            $user->sendConfirmEmail();
            Yii::$app->session->addFlash('success', Yii::t('app', 'Вы успешно зарегистрированы. На указанный e-mail отправлено письмо для подтверждения регистрации.'));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRegisterConfirm($auth_key)
    {
        $user = User::findOne(['auth_key' => $auth_key]);

        if ($user === null) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь не найден.'));

            return $this->redirect('/');
        }

        $user->auth_key = null;
        $user->status = User::STATUS_ACTIVE;

        if (!$user->save()) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось подтвердить регистрацию. Попробуйте еще раз или обратитесь в службу поддержки.'));

            return $this->redirect('/');
        }

        Yii::$app->user->login($user);
        Yii::$app->session->addFlash('success', Yii::t('app', 'Регистрация успешно подтверждена.'));

        return $this->redirect('/');
    }

    public function actionForgotPassword()
    {
        if (Yii::$app->request->isPost) {
            $email = Yii::$app->request->post('email');

            $user = User::findOne(['email' => $email]);

            if ($user === null) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь с указанным e-mail не найден.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            if ($user->status === User::STATUS_REGISTERED) {
                $user->sendConfirmEmail();
                Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь с указанным e-mail зарегистрирован, но не подтвержден. На указанный e-mail отправлено письмо для подтверждения регистрации.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            $user->auth_key = Yii::$app->security->generateRandomString();

            if (!$user->save()) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось восстановить пароль. Попробуйте еще раз или обратитесь в службку поддержки.'));

                return $this->redirect(Yii::$app->request->referrer);
            }

            Yii::$app->mailer->compose('default', [
                'content' => Yii::t('app', 'Здравствуйте!') . ' ' . Html::tag('br') . ' ' . Yii::t('app', 'Для восстановления пароля на сайте sociomics.net перейдите по') . ' ' . Html::a(Yii::t('app', 'ссылке'), Url::to('/password-recovery/' . $user->auth_key, true)) . '.',
            ])
                ->setTo($user->email)
                ->setSubject(Yii::t('app', 'Новый пароль'))
                ->send();

            Yii::$app->session->addFlash('success', Yii::t('app', 'На Ваш e-mail отправлено письмо с ссылкой для восстановления пароля.'));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPasswordRecovery()
    {
        if (Yii::$app->request->isPost) {
            $auth_key = Yii::$app->request->post('auth_key');
            $password = Yii::$app->request->post('password');

            $user = User::findOne(['auth_key' => $auth_key]);

            if ($user === null) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Пользователь не найден.'));

                return $this->redirect('/');
            }

            $user->auth_key = null;
            $user->password_hash = Yii::$app->security->generatePasswordHash($password);

            if (!$user->save()) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Ошибка изменения пароля. Попробуйте еще раз или обратитесь в службу поддержки.'));
            }

            Yii::$app->user->login($user);
            Yii::$app->session->addFlash('success', Yii::t('app', 'Пароль успешно изменен.'));
        }

        return $this->redirect('/');
    }

    public function actionChangeLanguage($language)
    {
        Yii::$app->session->set('language', $language);

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionFeedback()
    {
        $name = Yii::$app->request->post('name');
        $email = Yii::$app->request->post('email');
        $message = Yii::$app->request->post('message');
        $recaptcha = Yii::$app->request->post('g-recaptcha-response');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'secret' => Yii::$app->params['recaptcha2']['secretKey'],
            'response' => $recaptcha
        ]);

        $curlResponse = curl_exec($ch);
        $recaptchaResponse = json_decode($curlResponse, true);
        curl_close($ch);

        if ($recaptchaResponse['success'] !== true) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Докажите, что Вы не робот :)'));

            return $this->redirect(Yii::$app->request->referrer);
        }

        Yii::$app->mailer->compose('default', [
            'content' => Yii::t('app', 'Здравствуйте! Новое сообщение с формы обратной связи сайта sociomics.net:')
                . Html::tag('br')
                . Yii::t('app', 'Имя') . ': ' . $name . Html::tag('br')
                . Yii::t('app', 'E-mail') . ': ' . $email . Html::tag('br')
                . Yii::t('app', 'Сообщение') . ': ' . $message
        ])
            ->setTo(Yii::$app->params['supportEmail'])
            ->setSubject(Yii::t('app', 'Обратная связь'))
            ->send();

        Yii::$app->session->addFlash('success', Yii::t('app', 'Сообщение успешно отправлено. Мы свяжемся с Вами по указанному e-mail. Спасибо!'));

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSitemap() {
    	$this->layout = false;
   		$urls = [
   			Url::to('/', true),
   			Url::to('/search', true),
   			Url::to('/library', true),
   			Url::to('/terms-of-use', true),
   			Url::to('/copyright', true),
   			Url::to('/about', true),
   			Url::to('/support-project', true),
   			Url::to('/become-author', true),
   			Url::to('/trends', true),

   		];

   		$sociomicses = Sociomics::find()
	       ->where(['is_published' => 1])
	       ->all();

	    foreach ($sociomicses as $sociomics) {
	    	$urls[] = Url::to($sociomics->getUrl(), true);
	    }

        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');

   		return $this->renderPartial('sitemap', compact('urls'));
    }
}
