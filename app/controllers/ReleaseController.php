<?php

namespace app\controllers;

use app\models\data\Release;
use Yii;
use yii\web\Controller;

class ReleaseController extends Controller
{

    public function actionDelete()
    {
        $releaseId = Yii::$app->request->get('id');

        if ($releaseId) {
            try {
                $release = Release::findOne($releaseId);

                if ($release->delete()) {
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Выпуск успешно удален.'));
                }
            } catch (\Exception $e) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Невозможно удалить выпуск.') . ' ' . $e->getMessage());
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionMy()
    {
        $pageTitle = Yii::t('app', 'Мои выпуски');
        $releases = Release::find()
            ->where(['language' => Yii::$app->language])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('index', compact('pageTitle', 'releases'));
    }

    public function actionForm()
    {
        $releaseId = Yii::$app->request->get('id');

        if ($releaseId) {
            $release = Release::findOne($releaseId);
        } else {
            $release = new Release();
        }

        if (Yii::$app->request->isPost) {
            $name = Yii::$app->request->post('name');
            $isPublished = Yii::$app->request->post('is_published');
            $language = Yii::$app->request->post('language');
            $date = Yii::$app->request->post('date');

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $release->name = $name;
                $release->date = $date;
                $release->language = $language;
                $release->is_published = $isPublished;

                if (!$release->save()) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить выпуск.') . ' ' . implode(' ', $release->getFirstErrors()));

                    return $this->render('form', compact('release'));
                }

                $transaction->commit();
                Yii::$app->session->addFlash('success', $releaseId ? Yii::t('app', 'Выпуск успешно сохранен.') : Yii::t('app', 'Выпуск успешно добавлен.'));

                return $this->redirect('/my-releases');
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить выпуск.') . ' ' . $e->getMessage());
            }
        }

        return $this->render('form', compact('release'));
    }
}
