<?php

namespace app\controllers;

use app\models\data\User;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\UploadedFile;

class UserController extends Controller
{

    public function actionProfile()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        if (Yii::$app->request->isPost) {
            $name = Yii::$app->request->post('name');
            $password = Yii::$app->request->post('password');
            $avatar = UploadedFile::getInstanceByName('avatar');

            $user->name = $name;

            if (!empty($password)) {
                $user->password_hash = Yii::$app->security->generatePasswordHash($password);
                Yii::$app->session->addFlash('success', Yii::t('app', 'Пароль успешно изменен.'));
            }

            if ($avatar) {
                $avatarName = time() . '.' . $avatar->getExtension();
                $avatarPath = '/user/' . $user->id . '/avatar/';

                if (FileHelper::createDirectory(Yii::getAlias('@webroot/uploads') . $avatarPath)
                    && $avatar->saveAs(Yii::getAlias('@webroot/uploads') . $avatarPath . $avatarName)) {
                    $user->avatar = $avatarPath . $avatarName;
                }
            }

            if ($user->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Профиль успешно сохранен.'));

                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('profile', compact('user'));
    }
}
