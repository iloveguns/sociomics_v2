<?php

namespace app\controllers;

use Yii;
use yii\helpers\FileHelper;
use yii\web\Controller;
use app\models\data\Page;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class PageController extends Controller
{

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest || !Yii::$app->user->identity->is_admin) {
            throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена.'));
        }

        $pages = Page::find()->all();

        return $this->render('index', compact('pages'));
    }

    public function actionView($slug)
    {
        $page = Page::findOne([
            'slug' => $slug,
            'language' => Yii::$app->language
        ]);

        if ($page === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена.'));
        }

        return $this->render('view', compact('page'));
    }

    public function actionEdit($id)
    {
        $page = Page::findOne($id);

        if (Yii::$app->request->isPost) {
            $page->title = Yii::$app->request->post('title');
            $page->body = Yii::$app->request->post('body');
            $image = UploadedFile::getInstanceByName('image');

            if (!$page->save()) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить страницу.') . ' ' . implode(' ', $page->getFirstErrors()));

                return $this->render('edit', compact('page'));
            }

            if ($image) {
                $imageName = time() . '.' . $image->getExtension();
                $imagePath = '/page/' . $page->id . '/image/';

                if (FileHelper::createDirectory(Yii::getAlias('@webroot/uploads') . $imagePath)
                    && $image->saveAs(Yii::getAlias('@webroot/uploads') . $imagePath . $imageName)) {
                    $page->image = $imagePath . $imageName;

                    if (!$page->save()) {
                        Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить страницу.') . ' ' . implode(' ', $page->getFirstErrors()));

                        return $this->render('edit', compact('page'));
                    }
                }
            }

            Yii::$app->session->addFlash('success', Yii::t('app', 'Страница успешно сохранена.'));

            return $this->redirect('/page/index');
        }

        return $this->render('edit', compact('page'));
    }
}
