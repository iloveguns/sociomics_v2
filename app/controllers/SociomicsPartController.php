<?php

namespace app\controllers;

use app\models\data\Sociomics;
use Yii;
use yii\web\Controller;
use app\models\data\SociomicsPart;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class SociomicsPartController extends Controller
{

    public function actionGet($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sociomicsPart = SociomicsPart::findOne($id);

        return [
            'image' => $sociomicsPart->getImageUrl(),
            'header' => $sociomicsPart->header,
            'body' => $sociomicsPart->body,
            'order' => $sociomicsPart->order
        ];
    }

    public function actionGetAll($sociomics_id)
    {
        $sociomics = Sociomics::findOne($sociomics_id);

        return $this->renderPartial('//sociomics/part/form/_parts', compact('sociomics'));
    }

    public function actionDelete($id)
    {
        $sociomicsPart = SociomicsPart::findOne($id);
        $sociomicsId = $sociomicsPart->sociomics_id;
        $sociomicsPart->delete();
        $this->orderSociomicsParts($sociomicsId);
    }

    public function actionAdd()
    {
        $sociomicsId = Yii::$app->request->post('sociomics_id');
        $embedSociomicsId = Yii::$app->request->post('embed_sociomics_id');
        $sociomicsPartId = Yii::$app->request->post('sociomics_part_id');
        $header = Yii::$app->request->post('header');
        $image = UploadedFile::getInstanceByName('image');
        $body = Yii::$app->request->post('body');
        $order = Yii::$app->request->post('order');

        if ($order === '') {
            $order = SociomicsPart::find()
                    ->where(['sociomics_id' => $sociomicsId])
                    ->max('`order`') + 1;
        }

        if ($embedSociomicsId) {
            $sociomicsParts = SociomicsPart::find()
                ->where(['sociomics_id' => $embedSociomicsId])
                ->orderBy(['order' => SORT_ASC])
                ->all();

            foreach ($sociomicsParts as $sociomicsPart) {
                $newSociomicsPart = new SociomicsPart([
                    'sociomics_id' => $sociomicsId,
                    'header' => $sociomicsPart->header,
                    'image' => $sociomicsPart->image,
                    'body' => $sociomicsPart->body,
                    'order' => $order
                ]);

                $newSociomicsPart->save();
                $order++;
            }
        } else {
            if ($sociomicsPartId) {
                $sociomicsPart = SociomicsPart::findOne($sociomicsPartId);
            } else {
                $sociomicsPart = new SociomicsPart();
            }

            $sociomicsPart->sociomics_id = $sociomicsId;
            $sociomicsPart->header = $header;
            $sociomicsPart->body = $body;
            $sociomicsPart->order = $order;

            if (!$sociomicsPart->save()) {
                return 'Не удалось сохранить часть социомикса. ' . implode(' ', $sociomicsPart->getFirstErrors());
            }

            if ($image) {
                $imageName = time() . '.' . $image->getExtension();
                $imagePath = '/sociomics/' . $sociomicsId . '/parts/' . $sociomicsPart->id . '/image/';

                if (FileHelper::createDirectory(Yii::getAlias('@webroot/uploads') . $imagePath)
                    && $image->saveAs(Yii::getAlias('@webroot/uploads') . $imagePath . $imageName)) {
                    $sociomicsPart->image = $imagePath . $imageName;
                }

                $sociomicsPart->save();
            }
        }

        $this->orderSociomicsParts($sociomicsId);

        return 1;
    }

    private function orderSociomicsParts($sociomicsId)
    {
        $order = 0;
        foreach (SociomicsPart::find()
                     ->where(['sociomics_id' => $sociomicsId])
                     ->orderBy(['order' => SORT_ASC])
                     ->all() as $sociomicsPart) {
            $sociomicsPart->order = $order;
            $sociomicsPart->save();
            $order++;
        }
    }
}
