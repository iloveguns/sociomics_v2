<?php

namespace app\controllers;

use app\models\data\Author;
use app\models\data\Comment;
use app\models\data\CommentLike;
use app\models\data\Favorite;
use app\models\data\Page;
use app\models\data\SociomicsLike;
use app\models\data\UserHistory;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Controller;
use app\models\data\Sociomics;
use yii\web\Response;
use yii\web\UploadedFile;
use app\models\data\SociomicsView;

class SociomicsController extends Controller
{

    public function actionGetComments($sociomics_id, $sort = null)
    {
        $response = '';
        $sociomics = Sociomics::findOne($sociomics_id);
        $commentsQuery = Comment::find()
            ->where(['sociomics_id' => $sociomics->id])
            ->andWhere(['parent_id' => null]);

        switch ($sort) {
            case 'interesting':
                $commentsQuery->select([
                    'comment.*',
                    'likesCount' => (new Query())
                        ->select('COUNT(*)')
                        ->from(CommentLike::tableName())
                        ->where('comment_like.comment_id = comment.id')
                ])->orderBy(['likesCount' => SORT_DESC]);

                break;
            case 'new':
                $commentsQuery->orderBy(['id' => SORT_DESC]);

                break;
            default:
                $commentsQuery->orderBy(['id' => SORT_ASC]);

                break;
        }

        $comments = $commentsQuery->all();

        foreach ($comments as $comment) {
            $response .= $this->renderPartial('_comment', [
                'sociomics' => $sociomics,
                'comment' => $comment,
                'parentId' => $comment->id
            ]);

            foreach ($comment->child as $commentChild) {
                $response .= $this->renderPartial('_comment', [
                    'sociomics' => $sociomics,
                    'comment' => $commentChild,
                    'parentId' => $comment->id
                ]);
            }
        }

        return $response;
    }

    public function actionComments()
    {
        $sociomicses = Sociomics::find()
            ->leftJoin(Comment::tableName(), 'comment.sociomics_id = sociomics.id')
            ->where([
                'comment.user_id' => Yii::$app->user->id,
                'sociomics.is_published' => 1,
                'sociomics.language' => Yii::$app->language,
            ])
            ->orderBy(['comment.id' => SORT_DESC])
            ->all();

        return $this->render('comments', compact('sociomicses'));
    }

    public function actionComment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $sociomicsId = Yii::$app->request->post('sociomics_id');
        $parentId = Yii::$app->request->post('parent_id');
        $text = Yii::$app->request->post('text');

        $comment = new Comment([
            'sociomics_id' => $sociomicsId,
            'parent_id' => $parentId,
            'text' => $text,
            'user_id' => Yii::$app->user->id,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        if ($comment->save()) {
        }

        return [
            'error' => 0,
            'message' => Yii::t('app', 'Комментарий успешно добавлен.')
        ];
    }

    public function actionCommentLike($comment_id, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $comment = Comment::findOne($comment_id);

        $like = CommentLike::find()->where([
            'comment_id' => $comment->id,
            'user_id' => Yii::$app->user->id
        ])->one();

        if ($like === null) {
            $like = new CommentLike([
                'comment_id' => $comment->id,
                'user_id' => Yii::$app->user->id
            ]);
        }

        $like->value = $value;
        $like->save();

        $likeCount = CommentLike::find()->where([
            'comment_id' => $comment->id,
            'value' => 1
        ])->count();

        $dislikeCount = CommentLike::find()->where([
            'comment_id' => $comment->id,
            'value' => -1
        ])->count();

        return [
            'isLike' => (int)$like->value === 1,
            'likeCount' => $likeCount,
            'dislikeCount' => $dislikeCount
        ];
    }

    public function actionLike($sociomics_id, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sociomics = Sociomics::findOne($sociomics_id);

        $like = SociomicsLike::find()->where([
            'sociomics_id' => $sociomics->id,
            'user_id' => Yii::$app->user->id
        ])->one();

        if ($like === null) {
            $like = new SociomicsLike([
                'sociomics_id' => $sociomics->id,
                'user_id' => Yii::$app->user->id,
            ]);
        }

        $like->value = $value;
        $like->created_at = date('Y-m-d H:i:s');
        $like->save();

        $likeCount = SociomicsLike::find()->where([
            'sociomics_id' => $sociomics->id,
            'value' => 1
        ])->count();

        $dislikeCount = SociomicsLike::find()->where([
            'sociomics_id' => $sociomics->id,
            'value' => -1
        ])->count();

        return [
            'isLike' => (int)$like->value === 1,
            'likeCount' => $likeCount,
            'dislikeCount' => $dislikeCount
        ];
    }

    public function actionInFavorite($sociomics_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sociomics = Sociomics::findOne($sociomics_id);

        $favorite = Favorite::find()->where([
            'sociomics_id' => $sociomics->id,
            'user_id' => Yii::$app->user->id
        ])->one();

        if ($favorite) {
            $favorite->delete();
            $isFavorite = false;
        } else {
            $favorite = (new Favorite([
                'sociomics_id' => $sociomics->id,
                'user_id' => Yii::$app->user->id
            ]))->save();
            $isFavorite = true;
        }

        return [
            'isFavorite' => $isFavorite
        ];
    }

    public function actionView()
    {
        $sociomicsId = Yii::$app->request->get('id');
        $sociomics = Sociomics::findOne($sociomicsId);
        $fullSociomics = null;
        $otherSociomicses = null;
        $otherSociomicsIds = [];

        $sociomics->increaseViews();
        $sociomics->saveUserHistory();

        if ($sociomics->full_sociomics_id) {
            $fullSociomics = Sociomics::find()->where([
                'id' => $sociomics->full_sociomics_id,
                'is_published' => 1
            ])->one();

            $otherSociomicses = Sociomics::find()
                ->leftJoin(Sociomics::tableName() . ' as full_sociomics', 'full_sociomics.id = sociomics.full_sociomics_id')
                ->where([
                    'full_sociomics.is_published' => 0,
                    'sociomics.full_sociomics_id' => $sociomics->full_sociomics_id
                ])->all();

            $otherSociomicsIds = ArrayHelper::getColumn($otherSociomicses, 'id');
        }

        $sociomicsParts = $sociomics->getSociomicsParts()
            ->orderBy(['order' => SORT_ASC])
            ->all();

        $sociomicsChapters = $sociomics->getSociomicsParts()
            ->where(['not', ['header' => null]])
            ->andWhere(['not', ['header' => '']])
            ->orderBy(['order' => SORT_ASC])
            ->all();

        // Get recommended sociomicses by tags
        $sociomicsTags = explode(',', $sociomics->tags);
        $recommendedSociomicses = [];
        $recommendedSociomicsesTemp = [];
        $allSociomicses = Sociomics::find()
            ->where([
                'is_published' => 1,
                'language' => Yii::$app->language
            ])
            ->andWhere(['!=', 'id', $sociomics->id])
            ->andWhere(['not in', 'id', $otherSociomicsIds])
            ->all();

        foreach ($allSociomicses as $allSociomics) {
            $allSociomicsTags = explode(',', $allSociomics->tags);
            $similarTagCount = count(array_intersect($sociomicsTags, $allSociomicsTags));

            if ($similarTagCount > 0) {
                $recomendedSociomicsesTemp[] = [
                    'sociomics' => $allSociomics,
                    'tagCount' => $similarTagCount
                ];
            }
        }

        usort($recommendedSociomicsesTemp, function (array $a, array $b) {
            return $a['tagCount'] < $b['tagCount'];
        });

        foreach ($recommendedSociomicsesTemp as $recommendedSociomicsTemp) {
            $recommendedSociomicses[] = $recommendedSociomicsTemp['sociomics'];

            if (count($recommendedSociomicses) === 8) {
                break;
            }
        }

        $trendSociomicses = Sociomics::find()
            ->select([
                'sociomics.*',
                'likesCount' => (new Query())
                    ->select('COUNT(*)')
                    ->from(SociomicsLike::tableName())
                    ->where('sociomics_like.sociomics_id = sociomics.id')
                    ->andWhere(['>=', 'sociomics_like.created_at', date('Y-m-d H:i:s', strtotime('-7 days'))]),
                'viewsCount' => (new Query())
                    ->select('COUNT(*)')
                    ->from(SociomicsView::tableName())
                    ->where('sociomics_view.sociomics_id = sociomics.id')
                    ->andWhere(['>=', 'sociomics_view.created_at', date('Y-m-d H:i:s', strtotime('-7 days'))]),
            ])
            ->where([
                'sociomics.is_published' => 1,
                'sociomics.language' => Yii::$app->language
            ])
            ->andWhere(['!=', 'sociomics.id', $sociomics->id])
            ->andWhere(['not in', 'sociomics.id', $otherSociomicsIds])
            ->andWhere(['not in', 'sociomics.id', $recommendedSociomicses])
            ->orderBy(['(viewsCount + likesCount)' => SORT_DESC])
            ->limit(8)
            ->all();

        foreach ($trendSociomicses as $trendSociomics) {
            $recommendedSociomicses[] = $trendSociomics;
        }

        $comments = Comment::find()
            ->where(['sociomics_id' => $sociomics->id])
            ->andWhere(['parent_id' => null])
            ->orderBy(['id' => SORT_DESC])
            ->limit(2)
            ->all();

        $supportProjectPage = Page::findOne([
            'slug' => 'support-project',
            'language' => Yii::$app->language
        ]);

        return $this->render('view', compact('sociomics', 'fullSociomics', 'otherSociomicses', 'sociomicsParts', 'sociomicsChapters', 'recommendedSociomicses', 'comments', 'supportProjectPage'));
    }

    public function actionLibrary()
    {
        $sort = Yii::$app->request->get('sort');

        $sociomicsesQuery = Sociomics::find()
            ->where([
                'sociomics.in_library' => 1,
                'sociomics.is_published' => 1,
                'sociomics.language' => Yii::$app->language
            ]);

        if ($sort === 'rating') {
            $sociomicsesQuery
                ->select([
                    'sociomics.*',
                    'likesCount' => (new Query())
                        ->select('COUNT(*)')
                        ->from(SociomicsLike::tableName())
                        ->where('sociomics_like.sociomics_id = sociomics.id'),
                ])
                ->addOrderBy(['likesCount' => SORT_DESC]);
        } else {
            $sociomicsesQuery
                ->select([
                    'sociomics.*',
                    'viewsCount' => (new Query())
                        ->select('COUNT(*)')
                        ->from(SociomicsView::tableName())
                        ->where('sociomics_view.sociomics_id = sociomics.id')
                ]);

            if ($sort === 'views') {
                $sociomicsesQuery->addOrderBy(['viewsCount' => SORT_DESC]);
            } else {
                $sociomicsesQuery
                    ->orderBy(['sociomics.is_pinned_in_library' => SORT_DESC])
                    ->addOrderBy(['viewsCount' => SORT_DESC]);
            }
        }

        $sociomicses = $sociomicsesQuery->all();

        return $this->render('library', compact('sociomicses', 'sort'));
    }

    public function actionSearch()
    {
        $q = Yii::$app->request->get('q');

        $sociomicses = Sociomics::find()
            ->select([
                'sociomics.*',
                'viewsCount' => (new Query())
                    ->select('COUNT(*)')
                    ->from(SociomicsView::tableName())
                    ->where('sociomics_view.sociomics_id = sociomics.id')
            ])
            ->where([
                'sociomics.is_published' => 1,
                'sociomics.language' => Yii::$app->language
            ])
            ->andFilterWhere([
                'OR',
                ['like', 'sociomics.name', $q],
                ['like', 'sociomics.tags', $q],
            ])
            ->orderBy(['viewsCount' => SORT_DESC])
            ->all();

        return $this->render('search', compact('sociomicses', 'q'));
    }

    public function actionTrends()
    {
        $sociomicses = Sociomics::find()
            ->select([
                'sociomics.*',
                'likesCount' => (new Query())
                    ->select('COUNT(*)')
                    ->from(SociomicsLike::tableName())
                    ->where('sociomics_like.sociomics_id = sociomics.id')
                    ->andWhere(['>=', 'sociomics_like.created_at', date('Y-m-d H:i:s', strtotime('-7 days'))]),
                'viewsCount' => (new Query())
                    ->select('COUNT(*)')
                    ->from(SociomicsView::tableName())
                    ->where('sociomics_view.sociomics_id = sociomics.id')
                    ->andWhere(['>=', 'sociomics_view.created_at', date('Y-m-d H:i:s', strtotime('-7 days'))]),
            ])
            ->where([
                'sociomics.is_published' => 1,
                'sociomics.language' => Yii::$app->language
            ])
            ->orderBy(['(viewsCount + likesCount)' => SORT_DESC])
            ->all();

        return $this->render('trends', compact('sociomicses'));
    }

    public function actionHistory()
    {
        $sociomicses = Sociomics::find()
            ->leftJoin(UserHistory::tableName(), 'user_history.sociomics_id = sociomics.id')
            ->where([
                'user_history.user_id' => Yii::$app->user->id,
                'sociomics.is_published' => 1,
                'sociomics.language' => Yii::$app->language
            ])
            ->orderBy(['user_history.id' => SORT_DESC])
            ->all();

        return $this->render('history', compact('sociomicses'));
    }

    public function actionMy()
    {
        $sociomicses = Sociomics::find()
            ->where(['language' => Yii::$app->language])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('my', compact('sociomicses'));
    }

    public function actionDelete()
    {
        $sociomicsId = Yii::$app->request->get('id');

        if ($sociomicsId) {
            try {

                $sociomics = Sociomics::findOne($sociomicsId);

                if ($sociomics->delete()) {
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Социомикс успешно удален.'));
                }
            } catch (\Exception $e) {
                Yii::$app->session->addFlash('error', Yii::t('app', 'Невозможно удалить социомикс.') . ' ' . $e->getMessage());
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionFavorites()
    {
        $sociomicses = Sociomics::find()
            ->leftJoin(Favorite::tableName(), 'favorite.sociomics_id = sociomics.id')
            ->where([
                'favorite.user_id' => Yii::$app->user->id,
                'sociomics.is_published' => 1,
                'sociomics.language' => Yii::$app->language
            ])
            ->orderBy(['favorite.id' => SORT_DESC])
            ->all();

        return $this->render('favorites', compact('sociomicses'));
    }

    public function actionDeleteFromFavorites()
    {
        $sociomicsId = Yii::$app->request->get('id');

        $favorite = Favorite::findOne([
            'sociomics_id' => $sociomicsId,
            'user_id' => Yii::$app->user->id,
        ]);

        if ($favorite->delete()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Социомикс успешно удален из избранного.'));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionForm()
    {
        $sociomicsId = Yii::$app->request->get('id');

        if ($sociomicsId) {
            $sociomics = Sociomics::findOne($sociomicsId);
        } else {
            $sociomics = new Sociomics();
        }

        if (Yii::$app->request->isPost) {
            $name = Yii::$app->request->post('name');
            $language = Yii::$app->request->post('language');
            $fullSociomicsId = Yii::$app->request->post('full_sociomics_id');
            $releaseId = Yii::$app->request->post('release_id');
            $messageToReaders = Yii::$app->request->post('message_to_readers');
            $desc = Yii::$app->request->post('desc');
            $tags = Yii::$app->request->post('tags');
            $image = UploadedFile::getInstanceByName('image');
            $authorId = Yii::$app->request->post('author_id');
            $painterId = Yii::$app->request->post('painter_id');
            $authorName = Yii::$app->request->post('author_name');
            $authorVk = Yii::$app->request->post('author_vk');
            $authorFacebook = Yii::$app->request->post('author_facebook');
            $authorInstagram = Yii::$app->request->post('author_instagram');
            $authorTwitter = Yii::$app->request->post('author_twitter');
            $authorEmail = Yii::$app->request->post('author_email');
            $authorAvatar = UploadedFile::getInstanceByName('author_avatar');
            $painterName = Yii::$app->request->post('painter_name');
            $painterVk = Yii::$app->request->post('painter_vk');
            $painterFacebook = Yii::$app->request->post('painter_facebook');
            $painterInstagram = Yii::$app->request->post('painter_instagram');
            $painterTwitter = Yii::$app->request->post('painter_twitter');
            $painterEmail = Yii::$app->request->post('painter_email');
            $painterAvatar = UploadedFile::getInstanceByName('painter_avatar');
            $isPublished = Yii::$app->request->post('is_published');
            $isFullRelease = Yii::$app->request->post('is_full_release');
            $inLibrary = Yii::$app->request->post('in_library');
            $isPinnedInLibrary = Yii::$app->request->post('is_pinned_in_library');

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $sociomics->name = $name;
                $sociomics->language = $language;
                $sociomics->full_sociomics_id = $fullSociomicsId;
                $sociomics->release_id = $releaseId;
                $sociomics->message_to_readers = $messageToReaders;
                $sociomics->desc = $desc;
                $sociomics->tags = $tags;
                $sociomics->is_published = (int)$isPublished;
                $sociomics->is_full_release = (int)$isFullRelease;
                $sociomics->in_library = (int)$inLibrary;
                $sociomics->is_pinned_in_library = (int)$isPinnedInLibrary;

                if ($sociomics->is_published && empty($sociomics->published_at)) {
                    $sociomics->published_at = date('Y-m-d');
                }

                if (!$sociomics->save()) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить социомикс.') . ' ' . implode(' ', $sociomics->getFirstErrors()));

                    return $this->render('form', compact('sociomics'));
                }

                if ($image) {
                    $imageName = time() . '.' . $image->getExtension();
                    $imagePath = '/sociomics/' . $sociomics->id . '/image/';

                    if (FileHelper::createDirectory(Yii::getAlias('@webroot/uploads') . $imagePath)
                        && $image->saveAs(Yii::getAlias('@webroot/uploads') . $imagePath . $imageName)) {
                        $sociomics->image = $imagePath . $imageName;
                    }
                }

                if ($authorId) {
                    $author = Author::findOne($authorId);
                } else {
                    $author = Author::findOne(['name' => $authorName]);

                    if ($author === null) {
                        $author = new Author(['role' => Author::ROLE_AUTHOR]);
                    }

                    $author->name = $authorName;
                    $author->vk = $authorVk;
                    $author->facebook = $authorFacebook;
                    $author->instagram = $authorInstagram;
                    $author->twitter = $authorTwitter;
                    $author->email = $authorEmail;

                    if (!$author->save()) {
                        $transaction->rollBack();
                        Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить автора.') . ' ' . implode(' ', $author->getFirstErrors()));

                        return $this->render('form', compact('sociomics'));
                    }

                    if ($authorAvatar) {
                        $authorAvatarName = time() . '.' . $authorAvatar->getExtension();
                        $authorAvatarPath = '/author/' . $author->id . '/avatar/';

                        if (FileHelper::createDirectory(Yii::getAlias('@webroot/uploads') . $authorAvatarPath)
                            && $authorAvatar->saveAs(Yii::getAlias('@webroot/uploads') . $authorAvatarPath . $authorAvatarName)) {
                            $author->avatar = $authorAvatarPath . $authorAvatarName;
                        }

                        $author->save();
                    }
                }

                $sociomics->author_id = $author->id;

                if ($painterId) {
                    $painter = Author::findOne($painterId);
                } else {
                    $painter = Author::findOne(['name' => $painterName]);

                    if ($painter === null) {
                        $painter = new Author(['role' => Author::ROLE_PAINTER]);
                    }

                    $painter->name = $painterName;
                    $painter->vk = $painterVk;
                    $painter->facebook = $painterFacebook;
                    $painter->instagram = $painterInstagram;
                    $painter->twitter = $painterTwitter;
                    $painter->email = $painterEmail;

                    if (!$painter->save()) {
                        $transaction->rollBack();
                        Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить художника.') . ' ' . implode(' ', $painter->getFirstErrors()));

                        return $this->render('form', compact('sociomics'));
                    }

                    if ($painterAvatar) {
                        $painterAvatarName = time() . '.' . $painterAvatar->getExtension();
                        $painterAvatarPath = '/painter/' . $painter->id . '/avatar/';

                        if (FileHelper::createDirectory(Yii::getAlias('@webroot/uploads') . $painterAvatarPath)
                            && $painterAvatar->saveAs(Yii::getAlias('@webroot/uploads') . $painterAvatarPath . $painterAvatarName)) {
                            $painter->avatar = $painterAvatarPath . $painterAvatarName;
                        }

                        $painter->save();
                    }
                }

                $sociomics->painter_id = $painter->id;

                if (!$sociomics->save()) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить социомикс.') . ' ' . implode(' ', $sociomics->getFirstErrors()));

                    return $this->render('form', compact('sociomics'));
                }

                $transaction->commit();

                Yii::$app->session->addFlash('success', $sociomicsId ? Yii::t('app', 'Социомикс успешно сохранен.') : Yii::t('app', 'Социомикс успешно добавлен.'));

                return $this->redirect('/sociomicses/' . $sociomics->id . '/edit');
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash('error', Yii::t('app', 'Не удалось сохранить социомикс.') . ' ' . $e->getMessage());
            }
        }

        return $this->render('form', compact('sociomics'));
    }
}
