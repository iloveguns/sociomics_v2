<?php

use app\models\data\Release;
use app\models\data\User;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * @var $this \yii\web\View
 * @var $releases Release[]
 * @var $pageTitle string|null
 */

$this->title = $pageTitle ?? Yii::t('app', 'Выпуски');

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => Url::to('/release', true)
]);

$this->registerMetaTag([
    'property' => 'description',
    'content' => ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description')
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('/resources/img/layout/logo.png', true)
]);

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website'
]);

$this->params['breadcrumbs'][] = $this->title;
?>

<script type="application/ld+json">
{
    "@context": "schema.org",
    "@type": "WebPage",
    "name": "<?= $this->title ?>",
    "description": "<?= ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description') ?>",
    "publisher": {
        "@type": "Organization",
        "name": "Sociomics"
    }
}

</script>

<div class="my-releases">
    <div class="release__navigation__buttons"><a href="/releases/add">
            <?= Yii::t('app', 'Добавить выпуск') ?></a>
    </div>
    <?php if (count($releases) > 0): ?>
        <?php foreach ($releases as $release): ?>
            <?php $sociomicses = $release->getSociomics()->orderBy(['id' => SORT_DESC])->all(); ?>
            <div class="release__navigation">
                <h2><?= $release->name ?> / <?= Yii::$app->formatter->asDate($release->date) ?></h2>
                <?php if (User::isAdmin()): ?>
                    <div class="release__editing js-release-editing-menu-box">
                        <button class="release__editing-menu-button js-release-editing-menu-button">
                            <img
                                    src="/resources/img/homepage/button.svg"></button>
                        <div class="release__editing__dropdown-content js-release-editing-menu">
                            <ul class="release__editing__dropdown-content-list">
                                <li class="release__editing__dropdown-content-item"><a
                                            class="release__editing__dropdown-content-title"
                                            href="<?= $release->getEditUrl() ?>"><?= Yii::t('app', 'Редактировать') ?></a>
                                </li>
                                <li class="release__editing__dropdown-content-item"><a
                                            class="release__editing__dropdown-content-title"
                                            href="<?= $release->getDeleteUrl() ?>"><?= Yii::t('app', 'Удалить') ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="library__sociomics-list">
                <?php if (count($sociomicses) > 0): ?>
                    <?php foreach ($sociomicses as $sociomics): ?>
                        <?= $this->render('//sociomics/item/_library-item', compact('sociomics')) ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="library__sociomics-list-empty">
                        <p><?= Yii::t('app', 'Социомиксов нет.') ?></p>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="my-releases-empty">
            <p><?= Yii::t('app', 'Выпусков нет.') ?></p>
        </div>
    <?php endif; ?>
</div>