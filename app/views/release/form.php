<?php

use app\models\data\Release;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $release Release
 */

$this->title = $release->name ?? Yii::t('app', 'Добавить выпуск');
?>

<div class="sociomics-create">
    <h2><?= $this->title ?></h2>
    <form class="sociomics-form sociomics-form--create" method="post">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
               value="<?= Yii::$app->request->csrfToken ?>"/>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Название выпуска') ?></label>
                    <input type="text" name="name" required value="<?= $release->name ?>"
                           placeholder="<?= Yii::t('app', 'Введите название выпуска') ?>">
                </div>
            </div>
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Дата выпуска') ?></label>
                    <?= \yii\widgets\MaskedInput::widget([
                        'name' => 'date',
                        'mask' => '99.99.9999',
                        'value' => $release->date ? date('d.m.Y', strtotime($release->date)) : null,
                        'options' => [
                            'required' => 'required',
                            'placeholder' => Yii::t('app', 'Введите дату выпуска')
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Язык') ?></label>
                    <?= Html::dropDownList('language', $release->language, Yii::$app->params['languages'], [
                        'prompt' => Yii::t('app', 'Выберите язык'),
                        'required' => 'required'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group sociomics-form-group--checkbox">
                    <?= Html::checkbox('is_published', $release->is_published, [
                        'id' => 'checkbox-publish',
                    ]) ?>
                    <label for="checkbox-publish"><?= Yii::t('app', 'Опубликовать') ?></label>
                </div>
            </div>
        </div>
        <div class="sociomics-form-footer">
            <button type="submit"><?= Yii::t('app', 'Сохранить') ?></button>
        </div>
    </form>
</div>

