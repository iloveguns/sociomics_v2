<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 */
?>

<div class="sociomics-modal mfp-hide" id="feedback">
    <div class="sociomics-modal__form">
        <div class="sociomics-modal__form-header">
            <h2><?= Yii::t('app', 'Написать нам') ?></h2>
            <a class="popup-modal-dismiss" href="">
                <img alt="<?= Yii::t('app', 'Закрыть') ?>" src="/resources/img/layout/close.svg">
            </a>
        </div>
        <form class="sociomics-form" action="/feedback" method="post">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                   value="<?= Yii::$app->request->csrfToken ?>"/>
            <div class="sociomics-form-row">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Имя') ?></label>
                    <input type="text" name="name" required placeholder="<?= Yii::t('app', 'Введите Ваше имя') ?>">
                </div>
            </div>
            <div class="sociomics-form-row">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'E-mail') ?></label>
                    <input type="email" name="email" required
                           placeholder="<?= Yii::t('app', 'Введите e-mail для обратной связи') ?>">
                </div>
            </div>
            <div class="sociomics-form-row">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Сообщение') ?></label>
                    <textarea rows="6" name="message" required
                              placeholder="<?= Yii::t('app', 'Введите сообщение') ?>"></textarea>
                </div>
            </div>

            <div class="sociomics-form-row">
                <div class="sociomics-form-group">
                    <div class="g-recaptcha" data-sitekey="<?= Yii::$app->params['recaptcha2']['siteKey'] ?>"></div>
                </div>
            </div>

            <div class="sociomics-form-footer">
                <button type="submit"><?= Yii::t('app', 'Отправить') ?></button>
            </div>
        </form>
    </div>
</div>

<?php if (Yii::$app->user->isGuest): ?>
    <div class="sociomics-modal mfp-hide" id="registration">
        <div class="sociomics-modal__form">
            <div class="sociomics-modal__form-header">
                <h2><?= Yii::t('app', 'Регистрация') ?></h2><a class="popup-modal-dismiss" href=""><img
                            alt="<?= Yii::t('app', 'Закрыть') ?>" src="/resources/img/layout/close.svg"></a>
            </div>
            <form class="sociomics-form" action="/register" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->csrfToken ?>"/>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'E-mail') ?></label>
                        <input type="email" name="email" required placeholder="<?= Yii::t('app', 'Введите e-mail') ?>">
                    </div>
                </div>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Пароль') ?></label>
                        <input type="password" name="password" required
                               placeholder="<?= Yii::t('app', 'Введите пароль') ?>">
                    </div>
                </div>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Пароль еще раз') ?></label>
                        <input type="password" required placeholder="<?= Yii::t('app', 'Введите пароль еще раз') ?>">
                    </div>
                </div>

                <!-- uLogin -->
                <div id="uLogin_ea7b6302" data-uloginid="ea7b6302"></div>

                <p class="sociomics-form-info-text"><?= Yii::t('app', 'Регистрируясь, я подтверждаю, что ознакомился и согласен с') ?>
                    <a
                            href="/terms-of-use"><?= Yii::t('app', 'пользовательским соглашением') ?></a>.</p>
                <div class="sociomics-form-footer">
                    <button type="submit"><?= Yii::t('app', 'Зарегистрироваться') ?></button>
                </div>
            </form>
        </div>
    </div>
    <div class="sociomics-modal mfp-hide" id="login">
        <div class="sociomics-modal__form">
            <div class="sociomics-modal__form-header">
                <h2><?= Yii::t('app', 'Войти') ?></h2><a class="popup-modal-dismiss" href=""><img
                            alt="<?= Yii::t('app', 'Закрыть') ?>" src="/resources/img/layout/close.svg"></a>
            </div>
            <form class="sociomics-form" action="/login" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->csrfToken ?>"/>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'E-mail') ?></label>
                        <input type="email" name="email" required placeholder="<?= Yii::t('app', 'Введите e-mail') ?>">
                    </div>
                </div>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Пароль') ?></label>
                        <input type="password" name="password" required
                               placeholder="<?= Yii::t('app', 'Введите пароль') ?>">
                    </div>
                </div>
                <div class="sociomics-form-password-links"><a class="modal-link"
                                                              href="#registration"><?= Yii::t('app', 'Регистрация') ?></a><a
                            class="modal-link" href="#forgot-password"><?= Yii::t('app', 'Забыли пароль?') ?></a></div>

                <!-- uLogin -->
                <div id="uLogin_ea7b6302" data-uloginid="ea7b6302"></div>

                <div class="sociomics-form-footer">
                    <button type="submit"><?= Yii::t('app', 'Войти') ?></button>
                </div>
            </form>
        </div>
    </div>
    <div class="sociomics-modal mfp-hide" id="forgot-password">
        <div class="sociomics-modal__form">
            <div class="sociomics-modal__form-header">
                <h2><?= Yii::t('app', 'Восстановление пароля') ?></h2><a class="popup-modal-dismiss" href=""><img
                            alt="<?= Yii::t('app', 'Закрыть') ?>" src="/resources/img/layout/close.svg"></a>
            </div>
            <form class="sociomics-form" action="/forgot-password" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->csrfToken ?>"/>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'E-mail') ?></label>
                        <input type="email" name="email" required placeholder="<?= Yii::t('app', 'Введите e-mail') ?>">
                    </div>
                </div>
                <p class="sociomics-form-info-text"><?= Yii::t('app', 'На указанный e-mail будет отправлено письмо с ссылкой для восстановления пароля.') ?></p>
                <div class="sociomics-form-footer">
                    <button type="submit"><?= Yii::t('app', 'Отправить') ?></button>
                </div>
            </form>
        </div>
    </div>
<?php endif; ?>

<?php if (count(Yii::$app->session->allFlashes) > 0): ?>
    <?php foreach (Yii::$app->session->allFlashes as $flashKey => $flashes): ?>
        <div class="sociomics-modal mfp-hide" id="flash-message">
            <div class="sociomics-modal__form">
                <div class="sociomics-modal__form-header">
                    <h2>
                        <?php if ($flashKey === 'success'): ?>
                            <?= Yii::t('app', 'Успешно') ?>
                        <?php elseif ($flashKey === 'error'): ?>
                            <?= Yii::t('app', 'Ошибка') ?>
                        <?php elseif ($flashKey === 'warning'): ?>
                            <?= Yii::t('app', 'Предупреждение') ?>
                        <?php endif; ?>
                    </h2>
                    <a class="popup-modal-dismiss" href="">
                        <img alt="<?= Yii::t('app', 'Закрыть') ?>" src="/resources/img/layout/close.svg">
                    </a>
                </div>
                <?php foreach ($flashes as $flash): ?>
                    <?= Html::tag('p', $flash) ?>
                <?php endforeach; ?>
            </div>
        </div>
        <script>
            $.magnificPopup.open({
                items: {
                    src: '#flash-message'
                }
            });
        </script>
    <?php endforeach; ?>
<?php endif; ?>

<?php if (mb_stripos(Yii::$app->request->pathInfo, 'password-recovery') !== false): ?>
    <div class="sociomics-modal mfp-hide" id="password-recovery">
        <div class="sociomics-modal__form">
            <div class="sociomics-modal__form-header">
                <h2><?= Yii::t('app', 'Смена пароля') ?></h2><a class="popup-modal-dismiss" href=""><img
                            alt="<?= Yii::t('app', 'Закрыть') ?>" src="/resources/img/layout/close.svg"></a>
            </div>
            <form class="sociomics-form" action="/password-recovery" method="post">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                       value="<?= Yii::$app->request->csrfToken ?>"/>
                <input type="hidden" name="auth_key" value="<?= Yii::$app->request->get('auth_key') ?>">
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Новый пароль') ?></label>
                        <input type="password" name="password" required
                               placeholder="<?= Yii::t('app', 'Введите новый пароль') ?>">
                    </div>
                </div>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Новый пароль еще раз') ?></label>
                        <input type="password" required
                               placeholder="<?= Yii::t('app', 'Введите новый пароль еще раз') ?>">
                    </div>
                </div>
                <div class="sociomics-form-footer">
                    <button type="submit"><?= Yii::t('app', 'Сохранить') ?></button>
                </div>
            </form>
        </div>
    </div>

    <script>
        $.magnificPopup.open({
            items: {
                src: '#password-recovery'
            }
        });
    </script>
<?php endif; ?>

<div class="bottom__cookie-block js-cookie-modal">
    <p><?= Yii::t('app', 'Продолжая использовать сайт, вы соглашаетесь на') ?> <a
                href="/terms-of-use"><?= Yii::t('app', 'сбор файлов Cookie') ?></a>.</p>
    <a href="javascript:void(0);" class="js-close-cookie-modal"><?= Yii::t('app', 'Согласиться и закрыть') ?></a>
</div>