<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html xmlns:fb='http://ogp.me/ns/fb#' lang="<?= Yii::$app->language ?> ">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="yandex-verification" content="f34aee8e5246deb9"/>
    <link rel="icon" type="image/x-icon" href="/resources/img/layout/favicon.png"/>
    <meta property="og:title" content="<?= $this->title ?>"/>
    <meta property="og:locale" content="ru-RU">
    <meta property="og:site_name"
          content="<?= ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description') ?>"/>
    <meta property="og:url" content="<?= URL::current() ?>"/>

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="https://www.google.com/recaptcha/api.js?hl=<?= Yii::$app->language ?>"></script>
</head>
<body>
<?php $this->beginBody() ?>

<header class="header js-header">

    <div class="container">
        <div class="header-inner">
            <a class="header-burger js-header-burger" href="javascript:void(0);">
                <img alt="<?= Yii::t('app', 'Меню') ?>" src="/resources/img/layout/burger.svg">
            </a>
            <a class="header-logo" href="/">
                <img alt="Sociomics" src="/resources/img/layout/logo.svg">
                <span>Sociomics</span>
            </a>
            <form class="header-search js-header-search-form" action="/search" method="get">
                <a class="header-search-back js-header-search-back" href="javascript:void(0);">
                    <img alt="<?= Yii::t('app', 'Назад') ?>" src="/resources/img/layout/arrow-left.svg">
                </a>
                <label class="js-header-search-btn" for="header-search-input">
                    <img alt="<?= Yii::t('app', 'Поиск') ?>" src="/resources/img/layout/search.svg">
                </label>
                <input type="text" name="q" required id="header-search-input"
                       placeholder="<?= Yii::t('app', 'Введите поисковой запрос') ?>">
            </form>
            <div class="header-right">
                <a href="/trends">
                    <img alt="<?= Yii::t('app', 'В тренде') ?>"
                         src="/resources/img/layout/fire.svg"><span><?= Yii::t('app', 'В тренде') ?></span>
                </a>
                <?php if (Yii::$app->user->isGuest): ?>
                    <a class="modal-link" href="#login">
                        <img alt="<?= Yii::t('app', 'Войти') ?>"
                             src="/resources/img/layout/user.svg"><span><?= Yii::t('app', 'Войти') ?></span>
                    </a>
                <?php else: ?>
                    <a href="/profile">
                        <img alt="<?= Yii::t('app', 'Войти') ?>"
                             src="/resources/img/layout/user.svg"><span><?= Yii::$app->user->identity->email ?></span>
                    </a>
                    <a href="/logout" class="header-right-logout">
                        <img src="/resources/img/layout/logout.svg" alt="<?= Yii::t('app', 'Выйти') ?>">
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>

<div class="container">

    <div class="content js-content" id="content">
        <div class="left-menu js-left-menu">
            <ul class="left-menu-main">


                <li>
                    <a href="/">
                        <img alt="<?= Yii::t('app', 'Главная') ?>" src="/resources/img/layout/home.svg">
                        <span><?= Yii::t('app', 'Главная') ?></span>
                    </a>
                </li>
                <li>
                    <a href="/library">
                        <img alt="<?= Yii::t('app', 'Библиотека') ?>" src="/resources/img/layout/library.svg">
                        <span><?= Yii::t('app', 'Библиотека') ?></span>
                    </a>
                </li>
                <?php if (!Yii::$app->user->isGuest): ?>
                    <li>
                        <a href="/favorites">
                            <img alt="<?= Yii::t('app', 'Избранное') ?>" src="/resources/img/layout/favorite.svg">
                            <span><?= Yii::t('app', 'Избранное') ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="/history">
                            <img alt="<?= Yii::t('app', 'История') ?>" src="/resources/img/layout/history.svg">
                            <span><?= Yii::t('app', 'История') ?></span>
                        </a>
                    </li>
                    <li><a href="/comments">
                            <img alt="<?= Yii::t('app', 'Комментарии') ?>" src="/resources/img/layout/comments.svg">
                            <span><?= Yii::t('app', 'Комментарии') ?></span>
                        </a>
                    </li>
                    <li class="desktop-hidden">
                        <a href="/trends">
                            <img alt="<?= Yii::t('app', 'В тренде') ?>" src="/resources/img/layout/fire.svg">
                            <span><?= Yii::t('app', 'В тренде') ?></span>
                        </a>
                    </li>
                    <?php if (Yii::$app->user->identity->is_admin): ?>
                        <li>
                            <a href="/my-releases">
                                <span><?= Yii::t('app', 'Мои выпуски') ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="/my-sociomicses">
                                <span><?= Yii::t('app', 'Мои социомиксы') ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="/page/index">
                                <span><?= Yii::t('app', 'Мои страницы') ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>
            <div class="left-menu-bottom">
                <ul class="left-menu-additional">
                    <li><a href="/terms-of-use"><?= Yii::t('app', 'Условия пользования') ?></a></li>
                    <li><a href="/copyright"><?= Yii::t('app', 'Авторские права') ?></a></li>
                    <li><a href="/about"><?= Yii::t('app', 'О проекте') ?></a></li>
                    <li><a href="/support-project"><?= Yii::t('app', 'Поддержать проект') ?></a></li>
                    <li><a href="/become-author"><?= Yii::t('app', 'Стать автором') ?></a></li>
                </ul>
                <div class="left-menu-copying"><img alt="<?= Yii::t('app', 'Копирайт') ?>"
                                                    src="/resources/img/layout/lock.svg">
                    <p><?= Yii::t('app', 'Полное или частичное копирование материалов должно быть согласовано с редакцией сайта') ?></p>
                </div>

                <div class="left-menu-social">
                    <?= $this->render('//layouts/_social') ?>
                </div>

                <ul class="left-menu-langs">
                    <?php foreach (Yii::$app->params['languages'] as $languageKey => $languageLabel): ?>
                        <li class="<?= Yii::$app->language === $languageKey ? 'active' : '' ?>">
                            <a href="/change-language/<?= $languageKey ?>"><?= $languageLabel ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <div class="left-menu-feedback"><a class="modal-link"
                                                   href="#feedback"><?= Yii::t('app', 'Написать нам') ?></a></div>
                <p class="left-menu-rights"><?= date('Y') ?> <?= Yii::t('app', 'Все права защищены') ?></p>
            </div>
        </div>
        <div class="content-right">
            <div class="left-menu-bg js-left-menu-bg"></div>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'options' => ['class' => 'breadcrumb', 'itemscope' => true, 'itemtype' => 'http://schema.org/BreadcrumbList'],
                'itemTemplate' => '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">{link}</li>' . PHP_EOL,
                'activeItemTemplate' => '<li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">{link}</li>' . PHP_EOL,
            ])
            ?>
            <?= $content ?>
        </div>
    </div>
    <a class="scroll-to-top js-scroll-to-top js-animate-scroll" href="#content">
        <img alt="<?= Yii::t('app', 'Наверх') ?>" src="/resources/img/layout/arrow-top.svg">
    </a>
</div>

<?= $this->render('_modals') ?>
<?= $this->render('_metriks') ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
