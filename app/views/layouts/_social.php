<ul>
    <?php if (Yii::$app->language === 'ru-RU'): ?>
        <li><a href="https://vk.com/club199264936" target="_blank" rel="nofollow"><i
                        class="fa fa-vk"></i></a></li>
        <li><a href="https://www.instagram.com/sociomics/" target="_blank" rel="nofollow"><i
                        class="fa fa-instagram"></i></a></li>
    <?php else: ?>
        <li><a href="https://www.facebook.com/sociomics" target="_blank"
               rel="nofollow"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://www.instagram.com/gary.nisharg/" target="_blank" rel="nofollow"><i
                        class="fa fa-instagram"></i></a></li>
        <li><a href="https://twitter.com/HIntellexit" target="_blank" rel="nofollow"><i
                        class="fa fa-twitter"></i></a></li>
    <?php endif; ?>
</ul>