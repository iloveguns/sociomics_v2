<?php

use app\models\data\Sociomics;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * @var $this yii\web\View
 * @var $releases \app\models\data\Release[]
 * @var $librarySociomicses Sociomics[]
 * @var $sociomicses Sociomics[]
 */

$this->title = 'Sociomics';

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => Url::to('/', true)
]);

$this->registerMetaTag([
    'property' => 'description',
    'content' => ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description')
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('/resources/img/layout/logo.png', true)
]);

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website'
]);
?>

<script type="application/ld+json">
{
    "@context": "schema.org",
    "@type": "WebPage",
    "name": "<?= $this->title ?>",
    "description": "<?= ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description') ?>",
    "publisher": {
        "@type": "Organization",
        "name": "Sociomics"
    }
}

</script>

<div class="homepage">
    <h1><?= $this->title ?></h1>
    <div class="homepage__release-list">
        <?php if (count($releases) > 0): ?>
            <?php foreach ($releases as $release): ?>
                <?php $sociomicses = $release->getSociomics()
                    ->where(['is_published' => 1])
                    ->orderBy(['published_at' => SORT_DESC])
                    ->all(); ?>
                <div class="homepage__release-item">
                    <h2><?= $release->name ?> / <?= Yii::$app->formatter->asDate($release->date) ?></h2>
                    <div class="homepage__release-item__sociomics-list">
                        <?php if (count($sociomicses) > 0): ?>
                            <?php foreach ($sociomicses as $sociomics): ?>
                                <div class="homepage__release-item__sociomics-item">
                                    <a class="homepage__release-item__sociomics-item-card"
                                       href="<?= $sociomics->getUrl() ?>">
                                        <img alt="<?= $sociomics->name ?>" src="<?= $sociomics->getImageUrl() ?>">
                                    </a>
                                    <div class="homepage__release-item__sociomics-item-description">
                                        <div class="homepage__release-item__sociomics-item-description-left">
                                            <img class="homepage__release-item__sociomics-item-avatar"
                                                 alt="<?= $sociomics->author->name ?>"
                                                 src="<?= $sociomics->author->getAvatarUrl() ?>">
                                        </div>
                                        <div class="homepage__release-item__sociomics-item-description-right">
                                            <a class="homepage__release-item__sociomics-item-title"
                                               href="<?= $sociomics->getUrl() ?>">
                                                <?= $sociomics->name ?>
                                            </a>
                                            <div class="homepage__release-item__sociomics-item-author">
                                                <?= $sociomics->author->name ?>
                                            </div>
                                            <div class="homepage__release-item__sociomics-item__views-block">
                                                <span class="homepage__release-item__sociomics-item-views"><?= $sociomics->getViewsStr() ?></span>
                                                <!--                                                <span class="homepage__release-item__sociomics-item-point"></span>-->
                                                <span class="homepage__release-item__sociomics-item-date"><?php // $sociomics->getRelativeDate() ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p><?= Yii::t('app', 'Социомиксов нет.') ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <p><?= Yii::t('app', 'Выпусков нет.') ?></p>
        <?php endif; ?>
    </div>
    <div class="homepage-library js-right-content">
        <div class="homepage-library-inner js-right-content-inner">
            <h3><?= Yii::t('app', 'Библиотека') ?></h3>
            <div class="homepage-library-sociomics-list">
                <?php if (count($librarySociomicses) > 0): ?>
                    <?php foreach ($librarySociomicses as $librarySociomics): ?>
                        <div class="homepage-library__sociomics-item">
                            <a class="homepage-library__sociomics-item-img" href="<?= $librarySociomics->getUrl() ?>">
                                <img alt="<?= $librarySociomics->name ?>" src="<?= $librarySociomics->getImageUrl() ?>">
                            </a>
                            <div class="homepage-library__sociomics-item__description">
                                <div class="homepage-library__sociomics-item__description-left">
                                    <img alt="<?= $librarySociomics->author->name ?>"
                                         src="<?= $librarySociomics->author->getAvatarUrl() ?>">
                                </div>
                                <div class="homepage-library__sociomics-item__description-right">
                                    <a class="homepage-library__sociomics-item__description-title"
                                       href="<?= $librarySociomics->getUrl() ?>">
                                        <?= $librarySociomics->name ?>
                                    </a>
                                    <div class="homepage-library__sociomics-item__description-author"><?= $librarySociomics->author->name ?></div>
                                    <div class="homepage-library__sociomics-item__description-views-block">
                                        <span><?= $librarySociomics->getViewsStr() ?></span>
                                        <!--                                        <span class="homepage-library__sociomics-item__description-point"></span>-->
                                        <span><?php // $librarySociomics->getRelativeDate() ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>