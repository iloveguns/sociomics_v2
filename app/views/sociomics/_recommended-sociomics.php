<?php

/**
 * @var $this \yii\web\View
 * @var $sociomics \app\models\data\Sociomics
 */

?>

<div class="sociomics-detail-right-inner__recommendations-item">
    <a class="sociomics-detail-right-inner__recommendations-item-image"
       href="<?= $sociomics->getUrl() ?>">
        <img alt="<?= $sociomics->name ?>" src="<?= $sociomics->getImageUrl() ?>">
    </a>
    <div class="sociomics-detail-right-inner__recommendations-item__description">
        <div class="sociomics-detail-right-inner__recommendations-item__description-left">
            <img alt="<?= $sociomics->author->name ?? '' ?>"
                 class="sociomics-detail-right-inner__recommendations-item__description-avatar"
                 src="<?= $sociomics->author->getAvatarUrl() ?>">
        </div>
        <div class="sociomics-detail-right-inner__recommendations-item__description-right">
            <a class="sociomics-detail-right-inner__recommendations-item__description-title"
               href="<?= $sociomics->getUrl() ?>">
                <?= $sociomics->name ?>
            </a>
            <div class="sociomics-detail-right-inner__recommendations-item__description-author">
                <?= $sociomics->author->name ?>
            </div>
            <div class="sociomics-detail-right-inner__recommendations-item__description__views-block">
                <span><?= $sociomics->getViewsStr() ?></span>
                <!--                <span class="sociomics-detail-right-inner__recommendations-item__description-point"></span>-->
                <span><?php // $sociomics->getRelativeDate() ?></span>
            </div>
        </div>
    </div>
</div>
