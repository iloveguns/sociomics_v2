<?php

/**
 * @var $sociomicsPart \app\models\data\SociomicsPart
 * @var $order integer
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>

<div class="sociomics-detail-content__item" id="sociomics-detail-part-<?= $sociomicsPart->id ?>">
    <div class="sociomics-detail-content__item-inner">
        <?php if (!empty($sociomicsPart->body) || !empty($sociomicsPart->image)): ?>
            <div class="sociomics-detail-content__item-number">
                <hr>
                <span><?= $order ?></span>
            </div>
        <?php endif; ?>
        <?php if ($sociomicsPart->header): ?>
            <h2><?= $sociomicsPart->header ?></h2>
        <?php endif; ?>
        <?php if ($sociomicsPart->image): ?>
            <div class="sociomics-detail-content__item-image">
                <img alt="<?= $sociomicsPart->header ? Html::encode($sociomicsPart->header) : ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.altDesc') ?>"
                     src="<?= $sociomicsPart->getImageUrl() ?>">
            </div>
        <?php endif; ?>
        <?php if ($sociomicsPart->body): ?>
            <div class="sociomics-detail-content__item-text">
                <?= $sociomicsPart->body ?>
            </div>
        <?php endif; ?>
    </div>
</div>
