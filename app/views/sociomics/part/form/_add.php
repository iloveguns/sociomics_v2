<?php

/**
 * @var $sociomicsPart \app\models\data\SociomicsPart
 */
?>

<div class="sociomics-detail-content__item-dropdown">
    <div class="js-dropdown">
        <a class="sociomics-detail-content__item-dropdown-open js-dropdown-open">
            <img alt="Добавить" src="/resources/img/sociomics-form/plus.svg">
        </a>
        <ul class="js-dropdown-list" data-order="<?= !empty($sociomicsPart) ? ($sociomicsPart->order - 1) : null ?>">
            <li><a class="js-add-part" data-part="header"><?= Yii::t('app', 'Добавить часть') ?></a></li>
            <li><a class="js-add-part" data-part="page"><?= Yii::t('app', 'Добавить страницу') ?></a></li>
            <li><a class="js-add-part" data-part="sociomics"><?= Yii::t('app', 'Добавить социомикс') ?></a></li>
        </ul>
    </div>
</div>