<?php

/**
 * @var $sociomicsPart \app\models\data\SociomicsPart
 * @var $order integer
 */
?>

<?= $this->render('//sociomics/part/form/_add', compact('sociomicsPart')) ?>

<div class="sociomics-detail-content__item" id="sociomics-detail-part-<?= $sociomicsPart->id ?>">
    <div class="sociomics-detail-content__item-inner">
        <div class="sociomics-detail-content__item--controls">
            <a class="js-sociomics-part-edit" data-sociomics-part-id="<?= $sociomicsPart->id ?>"
               data-order="<?= $sociomicsPart->order ?>"
               data-part="<?= !empty($sociomicsPart->header) ? 'header' : 'page' ?>">
                <img alt="Редактировать" src="/resources/img/sociomics-form/edit.svg">
            </a>
            <a class="js-sociomics-part-remove" data-sociomics-part-id="<?= $sociomicsPart->id ?>">
                <img alt="Удалить" src="/resources/img/favorites/trash.svg">
            </a>
        </div>
        <?php if (!empty($sociomicsPart->body) || !empty($sociomicsPart->image)): ?>
            <div class="sociomics-detail-content__item-number">
                <hr>
                <span><?= $order ?></span>
            </div>
        <?php endif; ?>
        <?php if ($sociomicsPart->header): ?>
            <h2><?= $sociomicsPart->header ?></h2>
        <?php endif; ?>
        <?php if ($sociomicsPart->image): ?>
            <div class="sociomics-detail-content__item-image">
                <img alt="<?= $sociomicsPart->order ?>" src="<?= $sociomicsPart->getImageUrl() ?>">
            </div>
        <?php endif; ?>
        <?php if ($sociomicsPart->body): ?>
            <div class="sociomics-detail-content__item-text">
                <?= $sociomicsPart->body ?>
            </div>
        <?php endif; ?>
    </div>
</div>
