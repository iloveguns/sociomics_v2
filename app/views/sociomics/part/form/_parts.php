<?php

/**
 * @var $this \yii\web\View
 * @var $sociomics \app\models\data\Sociomics
 */
?>

<?php $order = 0; ?>
<?php foreach ($sociomics->getSociomicsParts()->orderBy(['order' => SORT_ASC])->all() as $sociomicsPart): ?>
    <?php $order += !empty($sociomicsPart->body) || !empty($sociomicsPart->image) ? 1 : 0; ?>
    <?= $this->render('//sociomics/part/form/_part', compact('sociomicsPart', 'order')) ?>
<?php endforeach; ?>
<?= $this->render('//sociomics/part/form/_add') ?>