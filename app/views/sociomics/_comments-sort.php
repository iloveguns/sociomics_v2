<?php

/**
 * @var $this \yii\web\View
 * @var $sociomics \app\models\data\Sociomics
 */

?>

<div class="sociomics-comments-item__comments__menu js-sociomics-comments-<?= $sociomics->id ?>">
    <select class="js-sort-comments" data-sociomics-id="<?= $sociomics->id ?>">
        <option value="new"><?= Yii::t('app', 'Сначала новые') ?></option>
        <option value="old"><?= Yii::t('app', 'Сначала старые') ?></option>
        <option value="interesting"><?= Yii::t('app', 'Сначала интересные') ?></option>
    </select>
    <div class="sociomics-comments-item__comments__menu-previous">
        <a href="javascript:void(0);"
           class="sociomics-comments-item__comments__menu-previous js-show-all-comments js-show-all-comments-<?= $sociomics->id ?>"
           data-sociomics-id="<?= $sociomics->id ?>">
            <?= Yii::t('app', 'Показать все комментарии') ?>
        </a>
    </div>
</div>