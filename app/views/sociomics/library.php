<?php

use app\models\data\Sociomics;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * @var $this \yii\web\View
 * @var $sort string
 * @var $sociomicses Sociomics[]
 */

$this->title = Yii::t('app', 'Библиотека');

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => Url::to('/library', true)
]);

$this->registerMetaTag([
    'property' => 'description',
    'content' => ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description')
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('/resources/img/layout/logo.png', true)
]);

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website'
]);

$this->params['breadcrumbs'][] = $this->title;
?>
<script type="application/ld+json">
{
    "@context": "schema.org",
    "@type": "WebPage",
    "name": "<?= $this->title ?>",
    "description": "<?= ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description') ?>",
    "publisher": {
        "@type": "Organization",
        "name": "Sociomics"
    }
}


</script>
<div class="library">
    <div class="library__navigation">
        <h1><?= $this->title ?></h1>
        <div class="library__navigation-list">
            <span><?= Yii::t('app', 'Сортировать по') ?>:</span>
            <a class="library__navigation-item <?= empty($sort) || $sort === 'views' ? 'library__navigation-item--active' : '' ?>"
               href="<?= Url::current(['sort' => 'views']) ?>"><?= Yii::t('app', 'просмотрам') ?></a>
            <a class="library__navigation-item <?= $sort === 'rating' ? 'library__navigation-item--active' : '' ?>"
               href="<?= Url::current(['sort' => 'rating']) ?>"><?= Yii::t('app', 'рейтингу') ?></a>
        </div>
    </div>
    <div class="library__sociomics-list">
        <?php if (count($sociomicses) > 0): ?>
            <?php foreach ($sociomicses as $sociomics): ?>
                <?= $this->render('//sociomics/item/_library-item', compact('sociomics')) ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="library__sociomics-list-empty">
                <p><?= Yii::t('app', 'В библиотеке нет социомиксов.') ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>
