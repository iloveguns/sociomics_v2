<?php

use app\models\data\Sociomics;
use yii\helpers\Html;
use app\models\data\Release;
use yii\helpers\ArrayHelper;
use app\models\data\Author;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $sociomics Sociomics
 */

$this->title = $sociomics->name ?? Yii::t('app', 'Добавить социомикс');
$this->params['breadcrumbs'][] = $this->title;

$author = Author::findOne($sociomics->author_id);
$painter = Author::findOne($sociomics->painter_id);
?>
<div class="sociomics-create">
    <h2><?= $this->title ?></h2>
    <form class="sociomics-form sociomics-form--create" method="post" enctype="multipart/form-data">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
               value="<?= Yii::$app->request->csrfToken ?>"/>
        <div class="sociomics-form-row">
            <div class="sociomics-form-group">
                <label><?= Yii::t('app', 'Название социомикса') ?></label>
                <input type="text" name="name" required value="<?= $sociomics->name ?>"
                       placeholder="<?= Yii::t('app', 'Введите название социомикса') ?>">
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Обложка') ?></label>
                    <div class="sociomics-form-group--img">
                        <?php if (!$sociomics->isNewRecord): ?>
                            <img src="<?= $sociomics->getImageUrl() ?>">
                        <?php endif; ?>
                        <input type="file" name="image" accept="image/*">
                    </div>
                </div>
            </div>
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Полный социомикс') ?></label>
                    <?= Html::dropDownList('full_sociomics_id', $sociomics->full_sociomics_id,
                        ArrayHelper::map(Sociomics::find()
                            ->where(['is_full_release' => 1])
                            ->orderBy(['name' => SORT_ASC])
                            ->all(), 'id', 'name'), [
                            'prompt' => Yii::t('app', 'Выберите полный социомикс'),
                        ]) ?>
                </div>
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Язык') ?></label>
                    <?= Html::dropDownList('language', $sociomics->language, Yii::$app->params['languages'], [
                        'prompt' => Yii::t('app', 'Выберите язык'),
                        'required' => 'required'
                    ]) ?>
                </div>
            </div>
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Выпуск') ?></label>
                    <?= Html::dropDownList('release_id', $sociomics->release_id,
                        ArrayHelper::map(Release::find()
                            ->orderBy(['date' => SORT_DESC])
                            ->all(), 'id', 'name'), [
                            'prompt' => Yii::t('app', 'Выберите выпуск'),
                        ]) ?>
                </div>
            </div>
        </div>
        <div class="sociomics--form-row">
            <div class="sociomics-form-group">
                <label><?= Yii::t('app', 'Послание читателям') ?></label>
                <textarea name="message_to_readers" required
                          placeholder="<?= Yii::t('app', 'Введите послание читателям') ?>"
                          rows="6"><?= $sociomics->message_to_readers ?></textarea>
            </div>
        </div>
        <div class="sociomics--form-row">
            <div class="sociomics-form-group">
                <label><?= Yii::t('app', 'Описание') ?></label>
                <textarea name="desc" required placeholder="<?= Yii::t('app', 'Введите описание') ?>"
                          rows="6"><?= $sociomics->desc ?></textarea>
            </div>
        </div>
        <div class="sociomics--form-row">
            <div class="sociomics-form-group">
                <label><?= Yii::t('app', 'Теги') ?></label>
                <textarea name="tags" required placeholder="<?= Yii::t('app', 'Введите теги через запятую') ?>"
                          rows="6"><?= $sociomics->tags ?></textarea>
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Автор') ?></label>
                    <?= Html::dropDownList('author_id', $sociomics->author_id,
                        ArrayHelper::map(Author::find()
                            ->where(['role' => Author::ROLE_AUTHOR])
                            ->orderBy(['name' => SORT_ASC])
                            ->all(), 'id', 'name'), [
                            'prompt' => Yii::t('app', 'Выберите автора'),
                        ]) ?>
                </div>
                <div class="js-author-col">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Имя автора') ?></label>
                        <input type="text" name="author_name" value="<?= $author->name ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите имя автора') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'ВК автора') ?></label>
                        <input type="url" name="author_vk" value="<?= $author->vk ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на ВК автора') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Facebook автора') ?></label>
                        <input type="url" name="author_facebook" value="<?= $author->facebook ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на Facebook автора') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Instagram автора') ?></label>
                        <input type="url" name="author_instagram" value="<?= $author->instagram ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на Instagram автора') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Twitter автора') ?></label>
                        <input type="url" name="author_twitter" value="<?= $author->twitter ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на Twitter автора') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'E-mail автора') ?></label>
                        <input type="email" name="author_email" value="<?= $author->email ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите e-mail автора') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Аватар автора') ?></label>
                        <div class="sociomics-form-group--img">
                            <?php if ($author): ?>
                                <img src="<?= $author->getAvatarUrl() ?>">
                            <?php endif; ?>
                            <input type="file" name="author_avatar" accept="image/*">
                        </div>
                    </div>
                </div>
            </div>
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Художник') ?></label>
                    <?= Html::dropDownList('painter_id', $sociomics->painter_id,
                        ArrayHelper::map(Author::find()
                            ->where(['role' => Author::ROLE_PAINTER])
                            ->orderBy(['name' => SORT_ASC])
                            ->all(), 'id', 'name'), [
                            'prompt' => Yii::t('app', 'Выберите художника'),
                        ]) ?>
                </div>
                <div class="js-painter-col">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Имя художника') ?></label>
                        <input type="text" name="painter_name" value="<?= $painter->name ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите имя художника') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'ВК художника') ?></label>
                        <input type="url" name="painter_vk" value="<?= $painter->vk ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на ВК художника') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Facebook художника') ?></label>
                        <input type="url" name="painter_facebook" value="<?= $painter->facebook ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на Facebook художника') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Instagram художника') ?></label>
                        <input type="url" name="painter_instagram" value="<?= $painter->instagram ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на Instagram художника') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Twitter художника') ?></label>
                        <input type="url" name="painter_twitter" value="<?= $painter->twitter ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите ссылку на Twitter художника') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'E-mail художника') ?></label>
                        <input type="email" name="painter_email" value="<?= $painter->email ?? '' ?>"
                               placeholder="<?= Yii::t('app', 'Введите e-mail художника') ?>">
                    </div>
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Аватар художника') ?></label>
                        <div class="sociomics-form-group--img">
                            <?php if ($painter): ?>
                                <img src="<?= $painter->getAvatarUrl() ?>">
                            <?php endif; ?>
                            <input type="file" name="painter_avatar" accept="image/*">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group sociomics-form-group--checkbox">
                    <?= Html::checkbox('is_published', $sociomics->is_published, [
                        'id' => 'checkbox-publish',
                    ]) ?>
                    <label for="checkbox-publish"><?= Yii::t('app', 'Опубликовать') ?></label>
                </div>
                <div class="sociomics-form-group sociomics-form-group--checkbox">
                    <?= Html::checkbox('is_full_release', $sociomics->is_full_release, [
                        'id' => 'checkbox-full-issue',
                    ]) ?>
                    <label for="checkbox-full-issue"><?= Yii::t('app', 'Полный выпуск') ?></label>
                </div>
            </div>
            <div class="sociomics-form-col">
                <div class="sociomics-form-group sociomics-form-group--checkbox">
                    <?= Html::checkbox('in_library', $sociomics->in_library, [
                        'id' => 'checkbox-library',
                    ]) ?>
                    <label for="checkbox-library"><?= Yii::t('app', 'В библиотеку') ?></label>
                </div>
                <div class="sociomics-form-group sociomics-form-group--checkbox">
                    <?= Html::checkbox('is_pinned_in_library', $sociomics->is_pinned_in_library, [
                        'id' => 'checkbox-pin-library',
                    ]) ?>
                    <label for="checkbox-pin-library"><?= Yii::t('app', 'Закреплен в библиотеке') ?></label>
                </div>
            </div>
        </div>
        <div class="sociomics-form-footer">
            <button type="submit"><?= Yii::t('app', 'Сохранить') ?></button>
        </div>
    </form>
</div>

<?php if (!$sociomics->isNewRecord): ?>
    <div class="sociomics-create">
        <h2><?= Yii::t('app', 'Части') ?></h2>
        <div class="sociomics-detail sociomics-detail--create">
            <div class="sociomics-detail-inner">
                <div class="sociomics-detail-content js-sociomics-parts">
                    <?= $this->render('//sociomics/part/form/_parts', compact('sociomics')) ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Добавить часть -->
    <div class="sociomics-modal mfp-hide" id="add-part-header">
        <div class="sociomics-modal__form">
            <div class="sociomics-modal__form-header">
                <h2>
                    <span class="js-sociomics-part-h2"><?= Yii::t('app', 'Добавить') ?></span> <?= Yii::t('app', 'часть') ?>
                </h2>
                <a class="popup-modal-dismiss" href="">
                    <img src="/resources/img/layout/close.svg">
                </a>
            </div>
            <form class="sociomics-form js-add-part-form" action="/sociomics-part/add" method="post">
                <input type="hidden" name="sociomics_id" value="<?= $sociomics->id ?>" class="js-part-sociomics-id">
                <input type="hidden" name="sociomics_part_id" class="js-part-sociomics-part-id">
                <input type="hidden" name="order" class="js-order">
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Название') ?></label>
                        <input type="text" name="header" required
                               class="js-part-header"
                               placeholder="<?= Yii::t('app', 'Введите название части') ?>">
                    </div>
                </div>
                <div class="sociomics-form-footer">
                    <button type="submit" class="js-sociomics-part-button"><?= Yii::t('app', 'Добавить') ?></button>
                </div>
            </form>
        </div>
    </div>

    <!-- Добавить страницу -->
    <div class="sociomics-modal mfp-hide" id="add-part-page">
        <div class="sociomics-modal__form sociomics-modal__form--wide">
            <div class="sociomics-modal__form-header">
                <h2>
                    <span class="js-sociomics-part-h2"><?= Yii::t('app', 'Добавить') ?></span> <?= Yii::t('app', 'страницу') ?>
                </h2>
                <a class="popup-modal-dismiss" href="">
                    <img src="/resources/img/layout/close.svg">
                </a>
            </div>
            <form class="sociomics-form js-add-part-form" action="/sociomics-part/add" method="post"
                  enctype="multipart/form-data">
                <input type="hidden" name="sociomics_id" value="<?= $sociomics->id ?>" class="js-part-sociomics-id">
                <input type="hidden" name="sociomics_part_id" class="js-part-sociomics-part-id">
                <input type="hidden" name="order" class="js-order">
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Изображение') ?></label>
                        <div class="sociomics-form-group--img">
                            <img src="" class="js-part-img" style="display: none;">
                            <input type="file" name="image" class="js-part-image" accept="image/*">
                        </div>
                    </div>
                </div>
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Текст') ?></label>
                        <textarea name="body" id="jsPartBody" placeholder="<?= Yii::t('app', 'Введите текст') ?>"
                                  rows="6"></textarea>
                    </div>
                </div>
                <div class="sociomics-form-footer">
                    <button type="submit" class="js-sociomics-part-button"><?= Yii::t('app', 'Добавить') ?></button>
                </div>
            </form>
        </div>
    </div>

    <!-- Добавить социомикс -->
    <div class="sociomics-modal mfp-hide" id="add-part-sociomics">
        <div class="sociomics-modal__form">
            <div class="sociomics-modal__form-header">
                <h2><?= Yii::t('app', 'Добавить социомикс') ?></h2>
                <a class="popup-modal-dismiss" href="">
                    <img src="/resources/img/layout/close.svg">
                </a>
            </div>
            <form class="sociomics-form js-add-part-form" action="/sociomics-part/add" method="post">
                <input type="hidden" name="sociomics_id" value="<?= $sociomics->id ?>" class="js-part-sociomics-id">
                <input type="hidden" name="order" class="js-order">
                <div class="sociomics-form-row">
                    <div class="sociomics-form-group">
                        <label><?= Yii::t('app', 'Социомикс') ?></label>
                        <?= Html::dropDownList('embed_sociomics_id', null, ArrayHelper::map(Sociomics::find()
                            ->orderBy(['name' => SORT_ASC])
                            ->all(), 'id', 'name'), [
                            'prompt' => Yii::t('app', 'Выберите социомикс'),
                            'class' => 'js-part-embed-sociomics-id',
                            'required' => 'required'
                        ]) ?>
                    </div>
                </div>
                <div class="sociomics-form-footer">
                    <button type="submit"><?= Yii::t('app', 'Добавить') ?></button>
                </div>
            </form>
        </div>
    </div>

    <script>
        var sociomicsId = '<?= $sociomics->id ?>';
        var sociomicsParts = $('.js-sociomics-parts');

        function reloadParts() {
            $.get('/sociomics-part/get-all', {sociomics_id: sociomicsId}, function (response) {
                sociomicsParts.html(response);
            });
        }

        $('.js-add-part-form').on('submit', function (e) {
            e.preventDefault();
            var _this = $(this);
            var sociomicsId = typeof _this.find('.js-part-sociomics-id').val() !== 'undefined' ? _this.find('.js-part-sociomics-id').val() : '';
            var embedSociomicsId = typeof _this.find('.js-part-embed-sociomics-id').val() !== 'undefined' ? _this.find('.js-part-embed-sociomics-id').val() : '';
            var sociomicsPartId = typeof _this.find('.js-part-sociomics-part-id').val() !== 'undefined' ? _this.find('.js-part-sociomics-part-id').val() : '';
            var header = _this.find('.js-part-header').length > 0 ? _this.find('.js-part-header').val() : '';
            var body = tinymce.get('jsPartBody') !== null ? tinymce.get('jsPartBody').getContent() : '';
            var order = _this.find('.js-order').val();
            var csrfToken = $('meta[name="csrf-token"]').attr("content");
            var image = _this.find('.js-part-image');

            var fd = new FormData;
            fd.append('_csrf', csrfToken);
            fd.append('sociomics_id', sociomicsId);
            fd.append('embed_sociomics_id', embedSociomicsId);
            fd.append('sociomics_part_id', sociomicsPartId);
            fd.append('header', header);
            fd.append('body', body);
            fd.append('order', order);

            if (image.prop('files')) {
                fd.append('image', image.prop('files')[0]);
            }

            $.ajax({
                url: _this.attr('action'),
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (response) {
                    reloadParts();
                    $.magnificPopup.close();
                }
            });
        });

        sociomicsParts.on('click', '.js-sociomics-part-remove', function () {
            var sociomicsPartId = $(this).data('sociomics-part-id');

            $.get('/sociomics-part/delete', {id: sociomicsPartId}, function (response) {
                reloadParts();
            });
        });

        sociomicsParts.on('click', '.js-add-part, .js-sociomics-part-edit', function () {
            var _this = $(this);
            var part = _this.data('part');
            var sociomicsPartId = _this.data('sociomics-part-id');
            var order = '';
            var partH2 = $('.js-sociomics-part-h2');
            var partButton = $('.js-sociomics-part-button');
            var partHeader = $('.js-part-header');
            var partImg = $('.js-part-img');
            var partBody = $('#jsPartBody');
            var partImage = $('.js-part-image');

            partImage.val('');

            if (sociomicsPartId) {
                $('.js-part-sociomics-part-id').val(sociomicsPartId);
                partH2.text('Редактировать');
                partButton.text('Сохранить');
                order = _this.data('order');

                $.get('/sociomics-part/get', {id: sociomicsPartId}, function (response) {
                    partHeader.val(response.header);
                    partImg.attr('src', response.image).show();
                    partBody.val(response.body);
                });
            } else {
                $('.js-part-sociomics-part-id').val('');
                order = _this.parents('.js-dropdown-list').data('order');
                partH2.text('Добавить');
                partButton.text('Добавить');
                partHeader.val('');
                partImg.attr('src', '').hide();
                partBody.val('');
            }

            $('.js-order').val(order);

            switch (part) {
                case 'header':
                    $.magnificPopup.open({
                        items: {
                            src: '#add-part-header'
                        },
                        closeOnBgClick: true,
                        showCloseBtn: false,
                    });

                    break;
                case 'page':
                    $.magnificPopup.open({
                        items: {
                            src: '#add-part-page'
                        },
                        closeOnBgClick: true,
                        showCloseBtn: false,
                        callbacks: {
                            close: function () {
                                tinymce.remove('#jsPartBody');
                            },
                            open: function () {
                                setTimeout(function () {
                                    tinymce.init({
                                        selector: '#jsPartBody',
                                        remove_linebreaks: false,
                                        force_p_newlines: true,
                                        force_br_newlines: true,
                                        plugins: 'link, paste',
                                    });
                                }, 500);
                            }
                        }
                    });
                    break;
                case 'sociomics':
                    $.magnificPopup.open({
                        items: {
                            src: '#add-part-sociomics'
                        },
                        closeOnBgClick: true,
                        showCloseBtn: false,
                        callbacks: {
                            close: function () {
                                $('.js-part-embed-sociomics-id option:selected').prop('selected', false);
                            }
                        }
                    });

                    break;
                default:
                    break;
            }
        });

        sociomicsParts.on('click', '.js-dropdown-open', function () {
            var _this = $(this);
            var dropdown = _this.parent();

            dropdown.find('.js-dropdown-list').toggle();
        });

        $(document).mouseup(function (e) {
            var dropdownOpenButton = $('.js-dropdown-open');
            var dropdownOpenMenu = dropdownOpenButton.parent().find('.js-dropdown-list');

            if (!dropdownOpenButton.is(e.target) && dropdownOpenButton.has(e.target).length === 0) {
                dropdownOpenMenu.hide();
            }
        });
    </script>
<?php endif; ?>

<script>
    $('select[name="author_id"]').change(function () {
        var _this = $(this);
        var col = $('.js-author-col');

        if (_this.val() === '') {
            col.show();
        } else {
            col.hide();
        }
    }).change();

    $('select[name="painter_id"]').change(function () {
        var _this = $(this);
        var col = $('.js-painter-col');

        if (_this.val() === '') {
            col.show();
        } else {
            col.hide();
        }
    }).change();
</script>

