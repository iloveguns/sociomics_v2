<?php

use app\models\data\Sociomics;

/**
 * @var $this \yii\web\View
 * @var $fullSociomics Sociomics
 */

?>

<?php if (!empty($fullSociomics)): ?>
    <div class="sociomics-detail-intro__left__full-version">
        <h4><?= Yii::t('app', 'Этот социомикс имеет полную версию') ?></h4>
        <div class="sociomics-detail-intro__left__full-version__release">
            <a class="sociomics-detail-intro__left__full-version__release-image"
               href="<?= $fullSociomics->getUrl() ?>">
                <img alt="<?= $fullSociomics->name ?>" src="<?= $fullSociomics->getImageUrl() ?>">
            </a>
            <div class="sociomics-detail-intro__left__full-version__release__information">
                <a class="sociomics-detail-intro__left__full-version__release__information-title"
                   href="<?= $fullSociomics->getUrl() ?>">
                    <?= $fullSociomics->name ?>
                </a>
                <div class="sociomics-detail-intro__left__full-version__release__information-author">
                    <img alt="<?= $fullSociomics->author->name ?? '' ?>"
                         src="<?= $fullSociomics->author->getAvatarUrl() ?>">
                    <span><?= $fullSociomics->author->name ?></span>
                </div>
                <div class="sociomics-detail-intro__left__full-version__release__information-views">
                    <span class="sociomics-detail-intro__left__full-version__release__information-views-number"><?= $fullSociomics->getViewsStr() ?></span>
                    <!--                    <span class="sociomics-detail-intro__left__full-version__release__information-views-point"></span>-->
                    <span class="sociomics-detail-intro__left__full-version__release__information-views-date"><?php // $fullSociomics->getRelativeDate() ?></span>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
