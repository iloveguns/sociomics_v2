<?php if (Yii::$app->language === 'ru-RU'): ?>
    <iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&amp;targets=%D0%9D%D0%B0%20%D0%BF%D0%BE%D0%B4%D0%B4%D0%B5%D1%80%D0%B6%D0%BA%D1%83%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0%20%22%D0%A1%D0%BE%D1%86%D0%B8%D0%BE%D0%BC%D0%B8%D0%BA%D1%81%22&amp;targets-hint=&amp;default-sum=&amp;button-text=14&amp;payment-type-choice=on&amp;hint=&amp;successURL=&amp;quickpay=shop&amp;account=410013952626051"
            width="100%" height="220" frameborder="0" allowtransparency="true"
            scrolling="none"></iframe>
<?php else: ?>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"
          style="text-align: center;">
        <input type="hidden" name="cmd" value="_s-xclick"/>
        <input type="hidden" name="hosted_button_id" value="PUAC5XJEJZ5L4"/>
        <input type="image"
               src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif"
               border="0" name="submit"
               title="PayPal - The safer, easier way to pay online!"
               alt="Donate with PayPal button"/>
        <img alt="" border="0" src="https://www.paypal.com/en_SK/i/scr/pixel.gif"
             width="1"
             height="1"/>
    </form>
<?php endif; ?>