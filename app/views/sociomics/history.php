<?php

use app\models\data\Sociomics;
use app\models\data\User;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $sociomicses Sociomics[]
 */

$this->title = Yii::t('app', 'История');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="favorites">
    <div class="favorites__nav">
        <h1><?= $this->title ?></h1>
    </div>
    <div class="favorites-inner">
        <?php if (count($sociomicses) > 0): ?>
            <?php foreach ($sociomicses as $sociomics): ?>
                <?= $this->render('item/_history-item', compact('sociomics')) ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="favorites-inner-empty">
                <p><?= Yii::t('app', 'Вы еще не читали ни одного социомикса.') ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>


