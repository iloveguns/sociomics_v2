<?php

/**
 * @var $this \yii\web\View
 * @var $sociomicsChapters \app\models\data\SociomicsPart[]
 */
?>

<?php if (!empty($sociomicsChapters)): ?>
    <div class="sociomics-detail-right-inner__content">
        <div class="sociomics-detail-right-inner__content-title"><?= Yii::t('app', 'Содержание') ?></div>
        <ul class="sociomics-detail-right-inner__content__list">
            <?php foreach ($sociomicsChapters as $sociomicsChapter): ?>
                <li>
                    <a href="#sociomics-detail-part-<?= $sociomicsChapter->id ?>" class="js-animate-scroll">
                        <?= $sociomicsChapter->header ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>