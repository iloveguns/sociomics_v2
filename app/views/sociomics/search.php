<?php

use app\models\data\Sociomics;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * @var $this \yii\web\View
 * @var $q string
 * @var $sociomicses Sociomics[]
 */

$this->title = Yii::t('app', 'Поиск социомиксов по фразе') . ' «' . Html::encode($q) . '»';

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => Url::to('/search', true)
]);

$this->registerMetaTag([
    'property' => 'description',
    'content' => ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description')
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('/resources/img/layout/logo.png', true)
]);

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website'
]);

$this->params['breadcrumbs'][] = $this->title;
?>
<script type="application/ld+json">
{
    "@context": "schema.org",
    "@type": "WebPage",
    "name": "<?= $this->title ?>",
    "description": "<?= ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description') ?>",
    "publisher": {
        "@type": "Organization",
        "name": "Sociomics"
    }
}

</script>

<div class="favorites">
    <div class="favorites__nav">
        <h1><?= $this->title ?></h1>
    </div>
    <div class="favorites-inner">
        <?php if (count($sociomicses) > 0): ?>
            <?php foreach ($sociomicses as $sociomics): ?>
                <?= $this->render('item/_history-item', compact('sociomics')) ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="favorites-inner-empty">
                <p><?= Yii::t('app', 'По запросу не найдено ни одного социомикса.') ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>


