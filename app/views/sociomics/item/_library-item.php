<?php

use app\models\data\Sociomics;

/**
 * @var $sociomics Sociomics
 */
?>

<div class="library__sociomics-item">
    <a class="library__sociomics-item-card" href="<?= $sociomics->getUrl() ?>">
        <img src="<?= $sociomics->getImageUrl() ?>">
    </a>
    <div class="library__sociomics-item-description">
        <div class="library__sociomics-item-description-left">
            <img class="library__sociomics-item-avatar" src="<?= $sociomics->author->getAvatarUrl() ?? '' ?>">
        </div>
        <div class="library__sociomics-item-description-right">
            <a class="library__sociomics-item-title" href="<?= $sociomics->getUrl() ?>"><?= $sociomics->name ?></a>
            <div class="library__sociomics-item-author"><?= $sociomics->author->name ?? '' ?></div>
            <div class="library__sociomics-item__views-block">
                <span><?= $sociomics->getViewsStr() ?></span>
                <!--                <span class="library__sociomics-item-point"></span>-->
                <span><?php // $sociomics->getRelativeDate() ?></span>
            </div>
        </div>
    </div>
</div>
