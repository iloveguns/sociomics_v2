<?php

use app\models\data\Sociomics;
use app\models\data\User;

/**
 * @var $sociomics Sociomics
 */
?>

<div class="sociomics-wide">
    <div class="sociomics-wide__left">
        <a href="<?= $sociomics->getUrl() ?>">
            <img class="sociomics-wide__left-img" src="<?= $sociomics->getImageUrl() ?>">
        </a>
        <div class="sociomics-wide__left-bottom">
            <div class="sociomics-wide__left-views"><?= $sociomics->getViewsStr() ?></div>
            <div class="sociomics-wide__left-likes">
                <div class="sociomics-wide__left-like">
                    <img src="/resources/img/sociomics-detail/comments.svg">
                    <span><?= $sociomics->getCommentsCount() ?></span>
                </div>
                <div class="sociomics-wide__left-like">
                    <img src="/resources/img/favorites/like.svg">
                    <span><?= $sociomics->getLikesCount() ?></span>
                </div>
                <div class="sociomics-wide__left-like">
                    <img src="/resources/img/favorites/dislike.svg">
                    <span><?= $sociomics->getDislikesCount() ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="sociomics-wide__right">
        <div class="sociomics-wide__right__header">
            <h3><a href="<?= $sociomics->getUrl() ?>"><?= $sociomics->name ?></a></h3>
            <div class="sociomics-wide__right__header-published">
                <?= $sociomics->is_published
                    ? Yii::t('app', 'Опубликован') . ' ' . Yii::$app->formatter->asDate($sociomics->published_at)
                    : Yii::t('app', 'Не опубликован')
                ?>
            </div>
            <div class="sociomics-wide__right__header-authors">
                <?php if ($sociomics->author_id): ?>
                    <div class="sociomics-wide__right__header__author">
                        <div class="sociomics-wide__right__header__author-sign"><?= Yii::t('app', 'Автор') ?></div>
                        <div class="sociomics-wide__right__header__author-inner">
                            <img src="<?= $sociomics->author->getAvatarUrl() ?>">
                            <div class="sociomics-wide__right__header__author-name">
                                <?= $sociomics->author->name ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($sociomics->painter_id): ?>
                    <div class="sociomics-wide__right__header__author">
                        <div class="sociomics-wide__right__header__author-sign"><?= Yii::t('app', 'Художник') ?></div>
                        <div class="sociomics-wide__right__header__author-inner">
                            <img src="<?= $sociomics->painter->getAvatarUrl() ?>">
                            <div class="sociomics-wide__right__header__author-name">
                                <?= $sociomics->painter->name ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="sociomics-wide__right__desc">
            <div class="sociomics-wide__right__desc-header"><?= Yii::t('app', 'Описание') ?></div>
            <p class="sociomics-wide__right__desc-text"><?= $sociomics->desc ?></p>
        </div>
    </div>
</div>
