<?php

use app\models\data\Sociomics;
use app\models\data\User;

/**
 * @var $this \yii\web\View
 * @var $sociomics Sociomics
 */
?>

<?= $this->render('//sociomics/item/_history-item', compact('sociomics')) ?>

<?= $this->render('//sociomics/_comments-sort', compact('sociomics')) ?>

<div class="sociomics-comments-item__comments">
    <div class="js-comment-list js-comment-list-<?= $sociomics->id ?>" data-sociomics-id="<?= $sociomics->id ?>">
        <?php foreach ($sociomics->getComments()
                           ->where(['parent_id' => null])
                           ->orderBy(['id' => SORT_DESC])
                           ->limit(2)
                           ->all() as $comment): ?>
            <?= $this->render('//sociomics/_comment', [
                'sociomics' => $sociomics,
                'comment' => $comment,
                'parentId' => $comment->id
            ]) ?>

            <?php foreach ($comment->getChild()
                               ->orderBy(['id' => SORT_DESC])
                               ->limit(2)
                               ->all() as $commentChild): ?>
                <?= $this->render('//sociomics/_comment', [
                    'sociomics' => $sociomics,
                    'comment' => $commentChild,
                    'parentId' => $comment->id
                ]) ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
    <div class="sociomics-detail-content__comment">
        <?= $this->render('//sociomics/_comment-form', compact('sociomics')) ?>
    </div>
</div>
