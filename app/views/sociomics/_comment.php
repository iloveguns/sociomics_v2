<?php

/**
 * @var $this \yii\web\View
 * @var $sociomics \app\models\data\Sociomics
 * @var $comment \app\models\data\Comment
 * @var $parentId int
 */

?>

<div class="sociomics-comments-item__comments-item <?= $comment->parent_id ? 'sociomics-comments-item__comments-item--answer' : '' ?>">
    <img alt="<?= Yii::t('app', 'Автор') ?>" class="sociomics-comments-item__comments-item-avatar"
         src="<?= $comment->user->getAvatarUrl() ?>">
    <div class="sociomics-comments-item__comments-item__content">
        <div class="div sociomics-comments-item__comments-item__content-name">
            <?= $comment->user->name ?>
        </div>
        <div class="sociomics-comments-item__comments-item__content-text">
            <?= $comment->text ?>
        </div>
        <div class="sociomics-comments-item__comments-item__content__bottom">
            <div class="sociomics-comments-item__comments-item__content__bottom-date">
                <?= $comment->getRelativeCreateTime() ?>
            </div>
            <?php if (!Yii::$app->user->isGuest): ?>
                <a class="sociomics-comments-item__comments-item__content__bottom-answer js-answer"
                   data-parent-id="<?= $parentId ?>" data-name="<?= $comment->user->name ?>"
                   data-sociomics-id="<?= $sociomics->id ?>"
                   href="javascript:void(0);"><?= Yii::t('app', 'Ответить') ?></a>
            <?php endif; ?>
            <div class="sociomics-comments-item__comments-item__content__bottom-likes">
                <a href="javascript:void(0);"
                   class="<?= !Yii::$app->user->isGuest ? 'js-comment-like' : 'no-cursor' ?> js-comment-like-<?= $comment->id ?> <?= $comment->isLiked() ? 'is-like' : '' ?>"
                   data-comment-id="<?= $comment->id ?>" data-value="1">
                    <img alt="<?= Yii::t('app', 'Нравится') ?>" src="/resources/img/sociomics-detail/like.svg">
                    <span class="js-comment-like-count-<?= $comment->id ?>"><?= $comment->getLikesCount() ?></span>
                </a>
                <a href="javascript:void(0);"
                   class="<?= !Yii::$app->user->isGuest ? 'js-comment-dislike' : 'no-cursor' ?> js-comment-dislike-<?= $comment->id ?> <?= $comment->isDisliked() ? 'is-like' : '' ?>"
                   data-comment-id="<?= $comment->id ?>" data-value="-1">
                    <img alt="<?= Yii::t('app', 'Не нравится') ?>" src="/resources/img/sociomics-detail/dislike.svg">
                    <span class="js-comment-dislike-count-<?= $comment->id ?>"><?= $comment->getDislikesCount() ?></span>
                </a>
            </div>
        </div>
    </div>
</div>

