<?php
/**
 * @var $this \yii\web\View
 * @var $sociomics \app\models\data\Sociomics
 */
?>

<p class="comment-form-message js-comment-message-<?= $sociomics->id ?>">Комментарий успешно добавлен.</p>
<form class="js-comment-form">
    <input type="hidden" name="sociomics_id" value="<?= $sociomics->id ?>" class="js-comment-sociomics-id">
    <input type="hidden" name="parent_id" class="js-comment-parent-<?= $sociomics->id ?>">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
           value="<?= Yii::$app->request->csrfToken ?>"/>
    <img alt="<?= Yii::t('app', 'Автор') ?>" src="/resources/img/sociomics-detail/icon.svg">
    <textarea required name="text" placeholder="<?= Yii::t('app', 'Введите комментарий') ?>"
              rows="4" class="js-comment-textarea-<?= $sociomics->id ?>"></textarea>
    <div class="sociomics-detail-content__comment__buttons">
        <button class="sociomics-detail-content__comment__buttons-send"
                type="submit"><?= Yii::t('app', 'Отправить') ?></button>
    </div>
</form>

<script>
    var csrfParam = $('meta[name="csrf-param"]').attr("content");
    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    $('.js-comment-list').on('click', '.js-answer', function () {
        var sociomicsId = $(this).data('sociomics-id');
        var textarea = $('.js-comment-textarea-' + sociomicsId);

        $('.js-comment-parent-' + sociomicsId).val($(this).data('parent-id'));
        textarea.val($(this).data('name') + ', ').focus();
        $('html, body').animate({scrollTop: (textarea.offset().top - 101) + 'px'});
    });

    $('.js-comment-list').on('click', '.js-comment-like, .js-comment-dislike', function () {
        var _this = $(this);
        var commentId = _this.data('comment-id')
        var value = _this.data('value');

        if (_this.hasClass('is-like')) {
            return false;
        }

        $.get('/sociomics/comment-like', {comment_id: commentId, value: value}, function (response) {
            $('.js-comment-like-' + commentId + ', .js-comment-dislike-' + commentId).removeClass('is-like');

            if (response.isLike) {
                $('.js-comment-like-' + commentId).addClass('is-like');
            } else {
                $('.js-comment-dislike-' + commentId).addClass('is-like');
            }

            $('.js-comment-like-count-' + commentId).text(response.likeCount);
            $('.js-comment-dislike-count-' + commentId).text(response.dislikeCount);
        });
    });

    $('.js-comment-form').submit(function () {
        var _this = $(this);
        var sociomicsId = _this.find('.js-comment-sociomics-id').val();
        var message = $('.js-comment-message-' + sociomicsId);

        $.post('/sociomics/comment', _this.serialize() + '&' + csrfParam + '=' + csrfToken, function (response) {
            _this.find('textarea').val('');
            message.text(response.message);
            message.css('visibility', 'visible');
            setTimeout(function () {
                message.css('visibility', 'hidden');
            }, 2000);

            $.get('/sociomics/get-comments', {sociomics_id: sociomicsId}, function (response) {
                $('.js-comment-list-' + sociomicsId).html(response);
                $('.js-show-all-comments-' + sociomicsId).css('display', 'none');
            });
        });

        return false;
    });

    $('.js-show-all-comments').click(function () {
        var _this = $(this);
        var sociomicsId = _this.data('sociomics-id');

        $.get('/sociomics/get-comments', {sociomics_id: sociomicsId}, function (response) {
            $('.js-comment-list-' + sociomicsId).html(response);
            _this.css('display', 'none');
        });
    });

    $('.js-sort-comments').change(function () {
        var _this = $(this);
        var sociomicsId = _this.data('sociomics-id');

        $.get('/sociomics/get-comments', {sociomics_id: sociomicsId, sort: _this.val()}, function (response) {
            $('.js-comment-list-' + sociomicsId).html(response);
            $('.js-show-all-comments-' + sociomicsId).css('display', 'none');
        });
    });
</script>