<?php

use app\models\data\Sociomics;
use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $sociomicses Sociomics[]
 */

$this->title = Yii::t('app', 'Мои социомиксы');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="favorites">
    <div class="favorites__nav">
        <h1><?= $this->title ?></h1>
        <div class="favorites__nav-buttons">
            <a href="/sociomicses/add"><?= Yii::t('app', 'Добавить социомикс') ?></a>
        </div>
    </div>
    <div class="favorites-inner">
        <?php if (count($sociomicses) > 0): ?>
            <?php foreach ($sociomicses as $sociomics): ?>
                <?= $this->render('item/_my-item', compact('sociomics')) ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="favorites-inner-empty">
                <p><?= Yii::t('app', 'Социомиксов нет.') ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>


