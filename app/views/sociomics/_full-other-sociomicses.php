<?php

/**
 * @var $this \yii\web\View
 * @var $otherSociomicses \app\models\data\Sociomics[]
 */

?>

<?php if (!empty($otherSociomicses) && count($otherSociomicses) > 1): ?>
    <div class="sociomics-detail-content__bottom__other">
        <h4><?= Yii::t('app', 'Другие части этого социомикса') ?></h4>
        <div class="sociomics-detail-content__bottom__other-items js-sociomics-detail-other-slider">
            <?php foreach ($otherSociomicses as $otherSociomics): ?>
                <div>
                    <div class="sociomics-detail-content__bottom__other-item">
                        <a class="sociomics-detail-content__bottom__other-item-image"
                           href="<?= $otherSociomics->getUrl() ?>">
                            <img alt="<?= $otherSociomics->name ?>" src="<?= $otherSociomics->getImageUrl() ?>">
                        </a>
                        <div class="sociomics-detail-content__bottom__other-item__information">
                            <a class="sociomics-detail-content__bottom__other-item__information-title"
                               href="<?= $otherSociomics->getUrl() ?>"><?= $otherSociomics->name ?></a>
                            <div class="sociomics-detail-content__bottom__other-item__information-author">
                                <img alt="<?= $otherSociomics->author->name ?? '' ?>"
                                     src="<?= $otherSociomics->author->getAvatarUrl() ?>">
                                <span><?= $otherSociomics->author->name ?></span>
                            </div>
                            <div class="sociomics-detail-content__bottom__other-item__information-views">
                                <span class="sociomics-detail-content__bottom__other-item__information-views-number"><?= $otherSociomics->getViewsStr() ?></span>
                                <!--                                <span class="sociomics-detail-content__bottom__other-item__information-views-point"></span>-->
                                <span class="sociomics-detail-content__bottom__other-item__information-views-date"><?php // $otherSociomics->getRelativeDate() ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>