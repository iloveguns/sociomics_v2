<?php

use app\models\data\Sociomics;
use app\models\data\SociomicsPart;
use app\models\data\Comment;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $sociomics Sociomics
 * @var $fullSociomics Sociomics
 * @var $otherSociomicses Sociomics[]
 * @var $sociomicsParts SociomicsPart[]
 * @var $sociomicsChapters SociomicsPart[]
 * @var $recommendedSociomicses Sociomics[]
 * @var $comments Comment[]
 * @var $supportProjectPage \app\models\data\Page
 */

$this->title = $sociomics->name;

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => $sociomics->getUrl()
]);

$this->registerMetaTag([
    'property' => 'description',
    'content' => $sociomics->desc
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $sociomics->getImageUrl()
]);

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'article'
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Библиотека'), 'url' => ['/library']];
$this->params['breadcrumbs'][] = $this->title;
?>

<script type="application/ld+json">
{
  "@context": "schema.org",
  "@type": "Article",
  "name": "<?= Html::encode($sociomics->name) ?>",
  "description": "<?= Html::encode($sociomics->desc) ?>",
  "image": "<?= Html::encode($sociomics->getImageUrl()) ?>",
  "publisher": {
    "@type": "Organization",
    "name": "Sociomics"
  }
}

</script>

<div class="sociomics-detail">
    <div class="sociomics-detail-inner">
        <div class="sociomics-detail-intro">
            <div class="sociomics-detail-intro__left">
                <img itemprop="url image" alt="<?= Html::encode($sociomics->name) ?>"
                     src="<?= $sociomics->getImageUrl() ?>">
                <div class="sociomics-detail-intro__left__bottom">
                    <div class="sociomics-detail-intro__left__bottom-views"><?= $sociomics->getViewsStr() ?></div>
                    <div class="sociomics-detail-intro__left__bottom-likes">
                        <a class="sociomics-detail-intro__left__bottom-likes-like js-animate-scroll"
                           href="#sociomics-detail-support">
                            <img itemprop="url image" alt="Комментарии"
                                 src="/resources/img/sociomics-detail/comments.svg">
                            <span><?= $sociomics->getCommentsCount() ?></span>
                        </a>
                        <a class="sociomics-detail-intro__left__bottom-likes-like <?= !Yii::$app->user->isGuest ? 'js-like' : 'no-cursor' ?> <?= $sociomics->isLiked() ? 'is-like' : '' ?>"
                           href="javascript:void(0);" data-value="1">
                            <img itemprop="url image" alt="Нравится" src="/resources/img/sociomics-detail/like.svg">
                            <span class="js-like-count"><?= $sociomics->getLikesCount() ?></span>
                        </a>
                        <a class="sociomics-detail-intro__left__bottom-likes-like <?= !Yii::$app->user->isGuest ? 'js-dislike' : 'no-cursor' ?> <?= $sociomics->isDisliked() ? 'is-like' : '' ?>"
                           href="javascript:void(0);" data-value="-1">
                            <img itemprop="url image" alt="Не нравится"
                                 src="/resources/img/sociomics-detail/dislike.svg">
                            <span class="js-dislike-count"><?= $sociomics->getDislikesCount() ?></span>
                        </a>
                    </div>
                </div>

                <?= $this->render('_full-version', compact('fullSociomics')) ?>
            </div>
            <div class="sociomics-detail-intro__right">
                <div class="sociomics-detail-intro__right__header">
                    <div class="sociomics-detail-intro-share">
                        <a href="">
                            <img itemprop="url image" alt="Поделиться" src="/resources/img/sociomics-detail/share.svg">
                        </a>
                        <div class="ya-share2" data-services="vkontakte,facebook,twitter,telegram"
                             data-url="<?= Url::to([$sociomics->getUrl(),], true) ?>"
                             data-description="<?= mb_substr($sociomics->desc, 0, 137) ?>..."
                             data-image="<?= $sociomics->getImageUrl() ?>"
                             data-title="<?= $sociomics->name ?>"></div>
                    </div>
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <a href="javascript:void(0);"
                           class="js-favorite <?= $sociomics->isFavorite() ? 'is-favorite' : '' ?>">
                            <img itemprop="url image" alt="Добавить в избранное"
                                 src="/resources/img/sociomics-detail/heart.svg">
                        </a>
                    <?php endif; ?>
                </div>
                <div class="sociomics-detail-intro__right-title">
                    <h1><?= $sociomics->name ?></h1>
                    <span><?php // Yii::t('app', 'Опубликовано') ?><?php // $sociomics->getRelativeDate() ?></span>
                </div>
                <div class="sociomics-detail-intro__right__makers">
                    <div class="sociomics-detail-intro__right__makers__author">
                        <span itemprop="author"><?= Yii::t('app', 'Автор') ?></span>
                        <div class="sociomics-detail-intro__right__makers__author-info">
                            <img itemprop="url image" alt="<?= Html::encode($sociomics->author->name) ?>"
                                 src="<?= $sociomics->author->getAvatarUrl() ?>">
                            <span><?= $sociomics->author->name ?></span>
                        </div>
                    </div>
                    <div class="sociomics-detail-intro__right__makers__author">
                        <span itemprop="painter"><?= Yii::t('app', 'Художник') ?></span>
                        <div class="sociomics-detail-intro__right__makers__author-info">
                            <img itemprop="url image" alt="<?= Html::encode($sociomics->painter->name) ?>"
                                 src="<?= $sociomics->painter->getAvatarUrl() ?>">
                            <span><?= $sociomics->painter->name ?></span>
                        </div>
                    </div>
                </div>
                <div class="sociomics-detail-intro__right__text">
                    <div class="sociomics-detail-intro__right__text__description">
                        <div class="sociomics-detail-intro__right__text__description-title"><?= Yii::t('app', 'Описание') ?></div>
                        <p class="sociomics-detail-intro__right__text__description-text"><?= $sociomics->desc ?></p>
                        <div class="sociomics-detail-intro__right__text__description-title"><?= Yii::t('app', 'Обращение автора') ?></div>
                        <p class="sociomics-detail-intro__right__text__description-text"><?= $sociomics->message_to_readers ?></p>
                    </div>
                </div>
                <div class="sociomics-detail-intro__right-end">
                    <a class="js-animate-scroll" href="#sociomics-detail-support"><?= Yii::t('app', 'В конец') ?></a>
                </div>
                <div class="sociomics-detail-intro__right-mobile">
                    <?= $this->render('_full-other-sociomicses', compact('otherSociomicses')) ?>
                    <?= $this->render('_full-version', compact('fullSociomics')) ?>
                    <?= $this->render('_chapters', compact('sociomicsChapters')) ?>
                </div>
            </div>
        </div>
        <div class="sociomics-detail-content">
            <?php $order = 0; ?>
            <?php foreach ($sociomicsParts as $sociomicsPart): ?>
                <?php $order += !empty($sociomicsPart->body) || !empty($sociomicsPart->image) ? 1 : 0; ?>
                <?= $this->render('part/_part', compact('sociomicsPart', 'order')) ?>
            <?php endforeach; ?>

            <div class="sociomics-detail-content__bottom">
                <div class="sociomics-detail-content-start">
                    <a class="js-animate-scroll" href="#content"><?= Yii::t('app', 'В начало') ?></a>
                </div>
                <div class="sociomics-detail-content-support" id="sociomics-detail-support">
                    <a class="js-animate-scroll" href="#support">
                        <?= Yii::t('app', 'Понравился социомикс?') ?> <br> <?= Yii::t('app', 'Поддержи наш проект!') ?>
                    </a>
                    <div class="sociomics-detail-content-support__social">
                        <p><?= Yii::t('app', 'Следите за нашим журналом в соцсетях:') ?> </p>
                        <?= $this->render('//layouts/_social') ?>
                    </div>
                </div>
                <?= $this->render('_full-other-sociomicses', compact('otherSociomicses')) ?>
                <?= $this->render('_full-version', compact('fullSociomics')) ?>
            </div>

            <?= $this->render('_comments-sort', compact('sociomics')) ?>
            <div class="sociomics-detail-content__comments-list js-comment-list js-comment-list-<?= $sociomics->id ?>"
                 data-sociomics-id="<?= $sociomics->id ?>">
                <?php foreach ($comments as $comment): ?>
                    <?= $this->render('_comment', [
                        'sociomics' => $sociomics,
                        'comment' => $comment,
                        'parentId' => $comment->id
                    ]) ?>

                    <?php foreach ($comment->getChild()
                                       ->orderBy(['id' => SORT_DESC])
                                       ->limit(2)
                                       ->all() as $commentChild): ?>
                        <?= $this->render('_comment', [
                            'sociomics' => $sociomics,
                            'comment' => $commentChild,
                            'parentId' => $comment->id
                        ]) ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
            <div class="sociomics-detail-content__comment">
                <?php if (!Yii::$app->user->isGuest): ?>
                    <?= $this->render('_comment-form', compact('sociomics')) ?>
                <?php else: ?>
                    <p style="color: #4F4C4C;"><?= Yii::t('app', 'Авторизуйтесь, чтобы оставить комментарий.') ?></p>
                <?php endif; ?>
            </div>
            <div class="sociomics-detail-content__footer" id="support">
                <h3><?= Yii::t('app', 'Поддержать проект') ?></h3>
                <?= $supportProjectPage->body ?>
                <?= $this->render('_support') ?>
            </div>
        </div>
    </div>
    <div class="sociomics-detail-right js-right-content">
        <div class="sociomics-detail-right-inner js-right-content-inner">

            <?= $this->render('_chapters', compact('sociomicsChapters')) ?>

            <?php if (!empty($otherSociomicses)): ?>
                <div class="sociomics-detail-right-inner__recommendations">
                    <div class="sociomics-detail-right-inner__recommendations-title"><?= Yii::t('app', 'Другие части этого социомикса') ?></div>
                    <?php foreach ($otherSociomicses as $otherSociomics): ?>
                        <?= $this->render('_recommended-sociomics', [
                            'sociomics' => $otherSociomics
                        ]) ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($recommendedSociomicses)): ?>
                <div class="sociomics-detail-right-inner__recommendations">
                    <div class="sociomics-detail-right-inner__recommendations-title"><?= Yii::t('app', 'Рекомендуем') ?></div>
                    <?php foreach ($recommendedSociomicses as $recommendedSociomics): ?>
                        <?= $this->render('_recommended-sociomics', [
                            'sociomics' => $recommendedSociomics
                        ]) ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    var sociomicsId = '<?= $sociomics->id ?>';

    $('.js-favorite').click(function () {
        var _this = $(this);
        $.get('/sociomics/in-favorite', {sociomics_id: sociomicsId}, function (response) {
            if (response.isFavorite) {
                _this.addClass('is-favorite');
            } else {
                _this.removeClass('is-favorite');
            }
        });
    });

    $('.js-like, .js-dislike').click(function () {
        var _this = $(this);
        var value = _this.data('value');

        if (_this.hasClass('is-like')) {
            return false;
        }

        $.get('/sociomics/like', {sociomics_id: sociomicsId, value: value}, function (response) {
            $('.js-like, .js-dislike').removeClass('is-like');

            if (response.isLike) {
                $('.js-like').addClass('is-like');
            } else {
                $('.js-dislike').addClass('is-like');
            }

            $('.js-like-count').text(response.likeCount);
            $('.js-dislike-count').text(response.dislikeCount);
        });

    });
</script>
