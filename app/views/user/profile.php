<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * @var $this \yii\web\View
 * @var $user \app\models\data\User
 */

$this->title = Yii::t('app', 'Мой профиль');

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => Url::to('/profile', true)
]);

$this->registerMetaTag([
    'property' => 'description',
    'content' => ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description')
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('/resources/img/layout/logo.png', true)
]);

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sociomics-create">
    <h2><?= Yii::t('app', 'Мой профиль') ?></h2>
    <form class="sociomics-form sociomics-form--create" action="/profile" method="post" enctype="multipart/form-data">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
               value="<?= Yii::$app->request->csrfToken ?>"/>
        <div class="sociomics-form-row">
            <div class="sociomics-form-group">
                <label><?= Yii::t('app', 'Имя') ?></label>
                <input type="text" name="name" required value="<?= $user->name ?>"
                       placeholder="<?= Yii::t('app', 'Введите имя') ?>">
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Новый пароль') ?></label>
                    <input type="password" name="password" placeholder="<?= Yii::t('app', 'Введите новый пароль') ?>">
                </div>
            </div>
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Новый пароль еще раз') ?></label>
                    <input type="password" placeholder="<?= Yii::t('app', 'Введите новый пароль еще раз') ?>">
                </div>
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Аватар') ?></label>
                    <div class="sociomics-form-group--img">
                        <img src="<?= $user->getAvatarUrl() ?>">
                        <input type="file" name="avatar" accept="image/*">
                    </div>
                </div>
            </div>
        </div>
        <div class="sociomics-form-footer">
            <button type="submit"><?= Yii::t('app', 'Сохранить') ?></button>
        </div>
    </form>
</div>
