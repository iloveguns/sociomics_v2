<?php

/**
 * @var $this \yii\web\View
 * @var $page \app\models\data\Page
 */

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$this->title = $page->title;

$this->registerMetaTag([
    'property' => 'description',
    'content' => ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description')
]);

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => Url::current()
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('/resources/img/layout/logo.png', true)
]);

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website'
]);

$this->params['breadcrumbs'][] = $this->title;
?>

<script type="application/ld+json">
{
    "@context": "schema.org",
    "@type": "WebPage",
    "name": "<?= $this->title ?>",
    "description": "<?= ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description') ?>",
    "publisher": {
        "@type": "Organization",
        "name": "Sociomics"
    }
}


</script>
<div class="text-page">
    <h1><?= $page->title ?></h1>
    <div class="text-page-inner">

        <img alt="<?= $page->title ?>" src="<?= $page->getImageUrl() ?>"/>
        <?= $page->body ?>
        <?php if ($page->slug === 'support-project'): ?>
            <?= $this->render('//sociomics/_support') ?>
        <?php endif; ?>
        <?php if ($page->slug === 'become-author'): ?>
            <div style="text-align: center; margin-top: 15px;">
                <a class="button modal-link" href="#feedback"><?= Yii::t('app', 'Написать нам') ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>


