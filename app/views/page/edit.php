<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $page \app\models\data\Page
 */

$this->title = Yii::t('app', 'Редактировать страницу') . ' ' . $page->title;
?>

<div class="sociomics-create">
    <h2><?= $this->title ?></h2>
    <form class="sociomics-form sociomics-form--create" method="post" enctype="multipart/form-data">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
               value="<?= Yii::$app->request->csrfToken ?>"/>
        <div class="sociomics-form-row">
            <div class="sociomics-form-group">
                <label><?= Yii::t('app', 'Заголовок') ?></label>
                <input type="text" name="title" required value="<?= $page->title ?>"
                       placeholder="<?= Yii::t('app', 'Введите заголовок страницы') ?>">
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-col">
                <div class="sociomics-form-group">
                    <label><?= Yii::t('app', 'Изображение') ?></label>
                    <div class="sociomics-form-group--img">
                        <?php if ($page->image): ?>
                            <img src="<?= $page->getImageUrl() ?>">
                        <?php endif; ?>
                        <input type="file" name="image" accept="image/*">
                    </div>
                </div>
            </div>
        </div>
        <div class="sociomics-form-row">
            <div class="sociomics-form-group">
                <label><?= Yii::t('app', 'Текст') ?></label>
                <textarea name="body" id="jsPartBody" placeholder="<?= Yii::t('app', 'Введите текст') ?>"
                          rows="6"><?= $page->body ?></textarea>
            </div>
        </div>
        <div class="sociomics-form-footer">
            <button type="submit"><?= Yii::t('app', 'Сохранить') ?></button>
        </div>
    </form>
</div>

<script>
    tinymce.init({
        selector: '#jsPartBody',
        remove_linebreaks: false,
        force_p_newlines: true,
        force_br_newlines: true,
        plugins: 'link, paste',
    });
</script>

