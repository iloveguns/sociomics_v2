<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * @var $this \yii\web\View
 * @var $pages \app\models\data\Page[]
 */

$this->title = Yii::t('app', 'Мои страницы');

$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => Url::to('/page', true)
]);

$this->registerMetaTag([
    'property' => 'description',
    'content' => ArrayHelper::getValue(Yii::$app->params, 'seo.' . Yii::$app->language . '.description')
]);

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="favorites">
    <div class="favorites__nav">
        <h1><?= $this->title ?></h1>
    </div>
    <div class="favorites-inner">
        <table>
            <thead>
            <tr>
                <th><?= Yii::t('app', 'Страница') ?></th>
                <th><?= Yii::t('app', 'Язык') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($pages as $page): ?>
                <tr>
                    <td><a href="<?= $page->getEditUrl() ?>"><?= $page->title ?></a></td>
                    <td><?= $page->language ?></td>
                    <td style="width: 25px;">
                        <a href="<?= $page->getEditUrl() ?>">
                            <img alt="<?= Html::encode($page->title) ?>" src="/resources/img/sociomics-form/edit.svg"
                                 style="width: 25px;">
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


