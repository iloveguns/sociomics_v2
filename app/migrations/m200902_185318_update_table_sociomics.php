<?php

use yii\db\Migration;

/**
 * Class m200902_185318_update_table_sociomics
 */
class m200902_185318_update_table_sociomics extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('sociomics', 'release_id', $this->integer()->comment('Выпуск'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200902_185318_update_table_sociomics cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_185318_update_table_sociomics cannot be reverted.\n";

        return false;
    }
    */
}
