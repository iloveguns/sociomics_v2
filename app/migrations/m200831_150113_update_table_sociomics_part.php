<?php

use yii\db\Migration;

/**
 * Class m200831_150113_update_table_sociomics_part
 */
class m200831_150113_update_table_sociomics_part extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sociomics_part', 'header', $this->string()->after('sociomics_id')->comment('Заголовок'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200831_150113_update_table_sociomics_part cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_150113_update_table_sociomics_part cannot be reverted.\n";

        return false;
    }
    */
}
