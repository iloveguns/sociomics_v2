<?php

use yii\db\Migration;

/**
 * Class m201004_200722_update_table_sociomics_view
 */
class m201004_200722_update_table_sociomics_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \app\models\data\SociomicsView::deleteAll();
        $this->dropForeignKey('fk_sociomics_view_user', 'sociomics_view');
        $this->dropColumn('sociomics_view', 'user_id');
        $this->addColumn('sociomics_view', 'session_id', $this->string()->notNull()->after('sociomics_id')->comment('ID сессии'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201004_200722_update_table_sociomics_view cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201004_200722_update_table_sociomics_view cannot be reverted.\n";

        return false;
    }
    */
}
