<?php

use yii\db\Migration;

/**
 * Class m200908_170452_create_table_sociomics_view
 */
class m200908_170452_create_table_sociomics_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sociomics_like', 'created_at', $this->dateTime()->comment('Время лайка'));
        $this->dropColumn('sociomics', 'views');

        $this->createTable('sociomics_view', [
            'id' => $this->primaryKey(),
            'sociomics_id' => $this->integer()->notNull()->comment('Социомикс'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'created_at' => $this->dateTime()->comment('Время просмотра')
        ]);

        $this->addForeignKey('fk_sociomics_view_sociomics', 'sociomics_view', 'sociomics_id', 'sociomics', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_sociomics_view_user', 'sociomics_view', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200908_170452_create_table_sociomics_view cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200908_170452_create_table_sociomics_view cannot be reverted.\n";

        return false;
    }
    */
}
