<?php

use yii\db\Migration;

/**
 * Class m200905_232216_update_table_user_history
 */
class m200905_232216_update_table_user_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_history', 'viewed_at', $this->dateTime()->notNull()->comment('Время просмотра'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200905_232216_update_table_user_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200905_232216_update_table_user_history cannot be reverted.\n";

        return false;
    }
    */
}
