<?php

use yii\db\Migration;

/**
 * Class m200821_103119_init
 */
class m200821_103119_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('page', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->comment('Заголовок'),
            'slug' => $this->string()->comment('Код'),
            'image' => $this->string()->comment('Изображение'),
            'body' => $this->text()->notNull()->comment('Текст'),
            'language' => $this->string()->notNull()->comment('Язык'),
        ]);

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Имя'),
            'email' => $this->string()->notNull()->comment('E-mail'),
            'password_hash' => $this->string()->notNull()->comment('Хэш-пароль'),
            'auth_key' => $this->string()->comment('Ключ авторизации'),
            'language' => $this->string()->notNull()->comment('Язык'),
            'status' => $this->tinyInteger()->notNull()->defaultValue(0)->comment('Статус'),
            'avatar' => $this->string()->comment('Аватара'),
            'is_admin' => $this->boolean()->notNull()->defaultValue(0)->comment('Админ'),
        ]);

        $this->createTable('release', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Название'),
            'date' => $this->date()->notNull()->comment('Дата'),
            'language' => $this->string()->notNull()->comment('Язык'),
            'is_published' => $this->boolean()->notNull()->defaultValue(0)->comment('Опубликован')
        ]);

        $this->createTable('author', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Имя'),
            'role' => $this->string()->notNull()->comment('Роль'),
            'vk' => $this->string()->comment('ВК'),
            'facebook' => $this->string()->comment('Facebook'),
            'instagram' => $this->string()->comment('Instagram'),
            'twitter' => $this->string()->comment('Twitter'),
            'email' => $this->string()->comment('E-mail'),
            'avatar' => $this->string()->comment('Аватар')
        ]);

        $this->createTable('sociomics', [
            'id' => $this->primaryKey(),
            'release_id' => $this->integer()->notNull()->comment('Выпуск'),
            'full_sociomics_id' => $this->integer()->comment('Полный выпуск социомикса'),
            'author_id' => $this->integer()->comment('Автор'),
            'painter_id' => $this->integer()->comment('Художник'),
            'name' => $this->string()->notNull()->comment('Название'),
            'slug' => $this->string()->comment('Код'),
            'language' => $this->string()->notNull()->comment('Язык'),
            'is_full_release' => $this->boolean()->notNull()->defaultValue(0)->comment('Полный выпуск'),
            'is_published' => $this->boolean()->notNull()->defaultValue(0)->comment('Опубликован'),
            'published_at' => $this->date()->comment('Дата публикации'),
            'in_library' => $this->boolean()->notNull()->defaultValue(0)->comment('В библиотеку'),
            'is_pinned_in_library' => $this->boolean()->notNull()->defaultValue(0)->comment('Закреплен в библиотеке'),
            'message_to_readers' => $this->text()->comment('Сообщение читателям'),
            'desc' => $this->text()->comment('Описание'),
            'tags' => $this->text()->comment('Теги'),
            'image' => $this->string()->comment('Изображение'),
            'views' => $this->integer()->notNull()->defaultValue(0)->comment('Просмотры'),
        ]);

        $this->addForeignKey('fk_sociomics_release', 'sociomics', 'release_id', 'release', 'id');
        $this->addForeignKey('fk_sociomics_author', 'sociomics', 'author_id', 'author', 'id');
        $this->addForeignKey('fk_sociomics_painter', 'sociomics', 'painter_id', 'author', 'id');
        $this->addForeignKey('fk_sociomics_full_sociomics', 'sociomics', 'full_sociomics_id', 'sociomics', 'id');

        $this->createTable('sociomics_like', [
            'id' => $this->primaryKey(),
            'sociomics_id' => $this->integer()->notNull()->comment('Социомикс'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'value' => $this->tinyInteger()->notNull()->defaultValue(1)->comment('Лайк -> 1/ дизлайк -> -1'),
        ]);

        $this->addForeignKey('fk_sociomics_like_sociomics', 'sociomics_like', 'sociomics_id', 'sociomics', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_sociomics_like_user', 'sociomics_like', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('idx_unique_sociomics_like_sociomics_user', 'sociomics_like', ['sociomics_id', 'user_id'], true);

        $this->createTable('favorite', [
            'id' => $this->primaryKey(),
            'sociomics_id' => $this->integer()->notNull()->comment('Социомикс'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
        ]);

        $this->addForeignKey('fk_favorite_sociomics', 'favorite', 'sociomics_id', 'sociomics', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_favorite_user', 'favorite', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('idx_unique_favorite_sociomics_user', 'favorite', ['sociomics_id', 'user_id'], true);

        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'sociomics_id' => $this->integer()->notNull()->comment('Социомикс'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'parent_id' => $this->integer()->comment('Родительский комментарий'),
            'text' => $this->text()->notNull()->comment('Текст'),
            'created_at' => $this->dateTime()->notNull()->comment('Дата создания')
        ]);

        $this->addForeignKey('fk_comment_sociomics', 'comment', 'sociomics_id', 'sociomics', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_comment_user', 'comment', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_comment_parent', 'comment', 'parent_id', 'comment', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('comment_like', [
            'id' => $this->primaryKey(),
            'comment_id' => $this->integer()->notNull()->comment('Комментарий'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'value' => $this->tinyInteger()->notNull()->defaultValue(1)->comment('Лайк -> 1/ дизлайк -> -1'),
        ]);

        $this->addForeignKey('fk_comment_like_comment', 'comment_like', 'comment_id', 'comment', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_comment_like_user', 'comment_like', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('sociomics_part', [
            'id' => $this->primaryKey(),
            'sociomics_id' => $this->integer()->notNull()->comment('Социомикс'),
            'image' => $this->string()->comment('Изображение'),
            'body' => $this->text()->comment('Текст')
        ]);

        $this->addForeignKey('fk_sociomics_part_sociomics', 'sociomics_part', 'sociomics_id', 'sociomics', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('user_history', [
            'id' => $this->primaryKey(),
            'sociomics_id' => $this->integer()->notNull()->comment('Социомикс'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
        ]);

        $this->addForeignKey('fk_user_history_sociomics', 'user_history', 'sociomics_id', 'sociomics', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_history_user', 'user_history', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200821_103119_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200821_103119_init cannot be reverted.\n";

        return false;
    }
    */
}
