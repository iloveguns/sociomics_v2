<?php

use yii\db\Migration;

/**
 * Class m201002_110901_update_table_user
 */
class m201002_110901_update_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \app\models\data\User::updateAll(['name' => 'Unknown'], ['name' => null]);
        $this->alterColumn('user', 'name', $this->string()->defaultValue('Unknown')->comment('Имя'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_110901_update_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_110901_update_table_user cannot be reverted.\n";

        return false;
    }
    */
}
