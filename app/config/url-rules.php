<?php

return [
    '/social-auth' => '/site/social-auth',
    '/register' => '/site/register',
    '/register-confirm/<auth_key>' => '/site/register-confirm',
    '/login' => '/site/login',
    '/logout' => '/site/logout',
    '/forgot-password' => '/site/forgot-password',
    '/password-recovery/<auth_key>' => '/site/index',
    '/password-recovery' => '/site/password-recovery',
    '/change-language/<language>' => '/site/change-language',
    '/profile' => '/user/profile',
    '/feedback' => '/site/feedback',

    '/sociomicses/add' => '/sociomics/form',
    '/sociomicses/<id>/edit' => '/sociomics/form',
    '/sociomicses/<id>/delete' => '/sociomics/delete',
    '/sociomicses/<id>/delete-from-favorites' => '/sociomics/delete-from-favorites',
    '/sociomicses/<id>-<slug>' => '/sociomics/view',
    '/my-sociomicses' => '/sociomics/my',
    '/favorites' => '/sociomics/favorites',
    '/history' => '/sociomics/history',
    '/trends' => '/sociomics/trends',
    '/search' => '/sociomics/search',
    '/library' => '/sociomics/library',
    '/comments' => '/sociomics/comments',

    '/releases/add' => '/release/form',
    '/releases/<id>/edit' => '/release/form',
    '/releases/<id>/delete' => '/release/delete',
    '/my-releases' => '/release/my',

    '/sitemap.xml' => '/site/sitemap',
    '/<slug>' => '/page/view',
];
