<?php

return [
    'languages' => [
        'ru-RU' => 'RU',
        'en-US' => 'EN'
    ],
    'recaptcha2' => [
        'siteKey' => '6LdGdM8ZAAAAAGfuAy1hpc1wqjJ4eDjrG-YokwIU',
        'secretKey' => '6LdGdM8ZAAAAAJg9YogYfTutHVNvxg6R94NBBNnP'
    ],
    'seo' => [
        'ru-RU' => [
            'description' => 'Социомиксы – это художественный журнал независимых авторов о новых прогрессивных идеях, которые могут изменить наш мир к лучшему.',
            'AltDesc' => 'Художественный журнал независимых авторов о новых прогрессивных идеях, которые могут изменить наш мир к лучшему'
        ],
        'en-US' => [
            'description' => 'Sociomics is the art magazine of independent authors about new progressive ideas that can change our world for the better.',
            'AltDesc' => 'Art magazine of independent authors about new progressive ideas that can change our world for the better'
        ],
    ],
];
