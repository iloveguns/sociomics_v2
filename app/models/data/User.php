<?php

namespace app\models\data;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string|null $name Имя
 * @property string $email E-mail
 * @property string $password_hash Хэш-пароль
 * @property string|null $auth_key Ключ авторизации
 * @property string $language Язык
 * @property int $status Статус
 * @property string|null $avatar Аватара
 * @property int $is_admin Админ
 *
 * @property Comment[] $comments
 * @property CommentLike[] $commentLikes
 * @property Favorite[] $favorites
 * @property Sociomics[] $sociomics
 * @property SociomicsLike[] $sociomicsLikes
 * @property Sociomics[] $sociomics0
 * @property UserHistory[] $userHistories
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_REGISTERED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password_hash', 'language'], 'required'],
            [['status', 'is_admin'], 'integer'],
            [['name', 'email', 'password_hash', 'auth_key', 'language', 'avatar'], 'string', 'max' => 255],
            ['name', 'default', 'value' => 'Unknown'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'E-mail',
            'password_hash' => 'Хэш-пароль',
            'auth_key' => 'Ключ авторизации',
            'language' => 'Язык',
            'status' => 'Статус',
            'avatar' => 'Аватара',
            'is_admin' => 'Админ',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Gets query for [[Comments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[CommentLikes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCommentLikes()
    {
        return $this->hasMany(CommentLike::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Favorites]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasMany(Favorite::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Sociomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSociomics()
    {
        return $this->hasMany(Sociomics::className(), ['id' => 'sociomics_id'])->viaTable('favorite', ['user_id' => 'id']);
    }

    /**
     * Gets query for [[SociomicsLikes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSociomicsLikes()
    {
        return $this->hasMany(SociomicsLike::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[Sociomics0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSociomics0()
    {
        return $this->hasMany(Sociomics::className(), ['id' => 'sociomics_id'])->viaTable('sociomics_like', ['user_id' => 'id']);
    }

    /**
     * Gets query for [[UserHistories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserHistories()
    {
        return $this->hasMany(UserHistory::className(), ['user_id' => 'id']);
    }

    public function sendConfirmEmail()
    {
        return Yii::$app->mailer->compose('default', [
            'content' => Yii::t('app', 'Здравствуйте!') . ' ' . Html::tag('br') . ' ' . Yii::t('app', 'Спасибо за регистрацию на сайте sociomics.net. Перейдите по') . ' ' . Html::a(Yii::t('app', 'ссылке'), Url::to('/register-confirm/' . $this->auth_key, true)) . ' ' . Yii::t('app', 'для подтверждения регистрации') . '.',
        ])
            ->setTo($this->email)
            ->setSubject(Yii::t('app', 'Подтверждение регистрации'))
            ->send();
    }

    public function getAvatarUrl()
    {
        return $this->avatar ? Url::to('/uploads/' . $this->avatar, true) : '/resources/img/sociomics-detail/author.svg';
    }

    public static function isAdmin()
    {
        return !Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin;
    }
}
