<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property int $sociomics_id Социомикс
 * @property int $user_id Пользователь
 * @property int|null $parent_id Родительский комментарий
 * @property string $text Текст
 * @property string $created_at Дата создания
 *
 * @property Comment $parent
 * @property Comment[] $child
 * @property Sociomics $sociomics
 * @property User $user
 * @property CommentLike[] $commentLikes
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sociomics_id', 'user_id', 'text', 'created_at'], 'required'],
            [['sociomics_id', 'user_id', 'parent_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['sociomics_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sociomics::className(), 'targetAttribute' => ['sociomics_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sociomics_id' => 'Социомикс',
            'user_id' => 'Пользователь',
            'parent_id' => 'Родительский комментарий',
            'text' => 'Текст',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Comment::className(), ['id' => 'parent_id']);
    }

    /**
     * Gets query for [[Comments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChild()
    {
        return $this->hasMany(Comment::className(), ['parent_id' => 'id']);
    }

    /**
     * Gets query for [[Sociomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSociomics()
    {
        return $this->hasOne(Sociomics::className(), ['id' => 'sociomics_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[CommentLikes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCommentLikes()
    {
        return $this->hasMany(CommentLike::className(), ['comment_id' => 'id']);
    }

    public function getRelativeCreateTime()
    {
        return date('d.m.Y, H:i', strtotime($this->created_at));
    }

    public function getLikesCount()
    {
        return $this->getCommentLikes()
            ->where(['value' => 1])
            ->count();
    }

    public function getDislikesCount()
    {
        return $this->getCommentLikes()
            ->where(['value' => -1])
            ->count();
    }

    public function isLiked()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return CommentLike::find()->where([
            'comment_id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'value' => 1
        ])->exists();
    }

    public function isDisliked()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return CommentLike::find()->where([
            'comment_id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'value' => -1
        ])->exists();
    }
}
