<?php

namespace app\models\data;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string $title Заголовок
 * @property string|null $slug Код
 * @property string|null $image Изображение
 * @property string $body Текст
 * @property string $language Язык
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'body', 'language'], 'required'],
            [['body'], 'string'],
            [['title', 'slug', 'image', 'language'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'slug' => 'Код',
            'image' => 'Изображение',
            'body' => 'Текст',
            'language' => 'Язык',
        ];
    }

    public function getEditUrl()
    {
        return Url::to(['/page/edit', 'id' => $this->id]);
    }

    public function getImageUrl()
    {
        return $this->image ? Url::to('/uploads/' . $this->image, true) : '';
    }
}
