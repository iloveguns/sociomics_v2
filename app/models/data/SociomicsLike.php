<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "sociomics_like".
 *
 * @property int $id
 * @property int $sociomics_id Социомикс
 * @property int $user_id Пользователь
 * @property int $value Лайк -> 1/ дизлайк -> -1
 * @property string $created_at
 *
 * @property Sociomics $sociomics
 * @property User $user
 */
class SociomicsLike extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sociomics_like';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sociomics_id', 'user_id', 'created_at'], 'required'],
            [['sociomics_id', 'user_id', 'value'], 'integer'],
            [['sociomics_id', 'user_id'], 'unique', 'targetAttribute' => ['sociomics_id', 'user_id']],
            [['sociomics_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sociomics::className(), 'targetAttribute' => ['sociomics_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['created_at', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sociomics_id' => 'Социомикс',
            'user_id' => 'Пользователь',
            'value' => 'Лайк -> 1/ дизлайк -> -1',
            'created_at' => 'Время лайка'
        ];
    }

    /**
     * Gets query for [[Sociomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSociomics()
    {
        return $this->hasOne(Sociomics::className(), ['id' => 'sociomics_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
