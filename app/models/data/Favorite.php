<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "favorite".
 *
 * @property int $id
 * @property int $sociomics_id Социомикс
 * @property int $user_id Пользователь
 *
 * @property Sociomics $sociomics
 * @property User $user
 */
class Favorite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'favorite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sociomics_id', 'user_id'], 'required'],
            [['sociomics_id', 'user_id'], 'integer'],
            [['sociomics_id', 'user_id'], 'unique', 'targetAttribute' => ['sociomics_id', 'user_id']],
            [['sociomics_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sociomics::className(), 'targetAttribute' => ['sociomics_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sociomics_id' => 'Социомикс',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * Gets query for [[Sociomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSociomics()
    {
        return $this->hasOne(Sociomics::className(), ['id' => 'sociomics_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
