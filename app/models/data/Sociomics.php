<?php

namespace app\models\data;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Url;

/**
 * @property int $id
 * @property int $release_id Выпуск
 * @property int|null $full_sociomics_id Полный выпуск социомикса
 * @property int|null $author_id Автор
 * @property int|null $painter_id Художник
 * @property string $name Название
 * @property string|null $slug Код
 * @property string $language Язык
 * @property int $is_full_release Полный выпуск
 * @property int $is_published Опубликован
 * @property string|null $published_at Дата публикации
 * @property int $in_library В библиотеку
 * @property int $is_pinned_in_library Закреплен в библиотеке
 * @property string|null $message_to_readers Сообщение читателям
 * @property string|null $desc Описание
 * @property string|null $tags Теги
 * @property string|null $image Изображение
 *
 * @property Comment[] $comments
 * @property Favorite[] $favorites
 * @property User[] $users
 * @property Author $author
 * @property Sociomics $fullSociomics
 * @property Sociomics[] $sociomics
 * @property Author $painter
 * @property Release $release
 * @property SociomicsLike[] $sociomicsLikes
 * @property User[] $users0
 * @property SociomicsPart[] $sociomicsParts
 * @property UserHistory[] $userHistories
 */
class Sociomics extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sociomics';
    }

    public function rules()
    {
        return [
            [['name', 'language'], 'required'],
            [['release_id', 'full_sociomics_id', 'author_id', 'painter_id', 'is_full_release', 'is_published', 'in_library', 'is_pinned_in_library'], 'integer'],
            [['published_at'], 'safe'],
            [['message_to_readers', 'desc', 'tags'], 'string'],
            [['name', 'slug', 'language', 'image'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Author::class, 'targetAttribute' => ['author_id' => 'id']],
            [['full_sociomics_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sociomics::class, 'targetAttribute' => ['full_sociomics_id' => 'id']],
            [['painter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Author::class, 'targetAttribute' => ['painter_id' => 'id']],
            [['release_id'], 'exist', 'skipOnError' => true, 'targetClass' => Release::class, 'targetAttribute' => ['release_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'immutable' => false,
                'ensureUnique' => true,
                'skipOnEmpty' => true,
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'release_id' => 'Выпуск',
            'full_sociomics_id' => 'Полный выпуск социомикса',
            'author_id' => 'Автор',
            'painter_id' => 'Художник',
            'name' => 'Название',
            'slug' => 'Код',
            'language' => 'Язык',
            'is_full_release' => 'Полный выпуск',
            'is_published' => 'Опубликован',
            'published_at' => 'Дата публикации',
            'in_library' => 'В библиотеку',
            'is_pinned_in_library' => 'Закреплен в библиотеке',
            'message_to_readers' => 'Сообщение читателям',
            'desc' => 'Описание',
            'tags' => 'Теги',
            'image' => 'Изображение',
        ];
    }

    public function getComments()
    {
        return $this->hasMany(Comment::class, ['sociomics_id' => 'id']);
    }

    public function getFavorites()
    {
        return $this->hasMany(Favorite::class, ['sociomics_id' => 'id']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('favorite', ['sociomics_id' => 'id']);
    }

    public function getAuthor()
    {
        return $this->hasOne(Author::class, ['id' => 'author_id']);
    }

    public function getFullSociomics()
    {
        return $this->hasOne(Sociomics::class, ['id' => 'full_sociomics_id']);
    }

    public function getSociomics()
    {
        return $this->hasMany(Sociomics::class, ['full_sociomics_id' => 'id']);
    }

    public function getPainter()
    {
        return $this->hasOne(Author::class, ['id' => 'painter_id']);
    }

    public function getRelease()
    {
        return $this->hasOne(Release::class, ['id' => 'release_id']);
    }

    public function getSociomicsLikes()
    {
        return $this->hasMany(SociomicsLike::class, ['sociomics_id' => 'id']);
    }

    public function getSociomicsParts()
    {
        return $this->hasMany(SociomicsPart::class, ['sociomics_id' => 'id']);
    }

    public function getUserHistories()
    {
        return $this->hasMany(UserHistory::class, ['sociomics_id' => 'id']);
    }

    public function getImageUrl()
    {
        return $this->image ? Url::to('/uploads/' . $this->image, true) : '';
    }

    public function getViewsStr()
    {
        return Yii::$app->language === 'ru-RU' ?
            \MessageFormatter::formatMessage(Yii::$app->language, '{n, plural, one{# просмотр} few{# просмотра} many{# просмотров} other{# просмотров}}', ['n' => $this->getViewsCount()])
            : \MessageFormatter::formatMessage(Yii::$app->language, '{n, plural, one{# view} few{# views} many{# views} other{# views}}', ['n' => $this->getViewsCount()]);
    }

    public function getViewsCount()
    {
        return SociomicsView::find()
            ->where(['sociomics_id' => $this->id])
            ->count();
    }

    public function getCommentsCount()
    {
        return $this->getComments()->count();
    }

    public function getLikesCount()
    {
        return $this->getSociomicsLikes()
            ->where(['value' => 1])
            ->count();
    }

    public function getDislikesCount()
    {
        return $this->getSociomicsLikes()
            ->where(['value' => -1])
            ->count();
    }

    public function getEditUrl()
    {
        return Url::to('/sociomicses/' . $this->id . '/edit');
    }

    public function getDeleteUrl()
    {
        return Url::to('/sociomicses/' . $this->id . '/delete');
    }

    public function getDeleteFromFavoritesUrl()
    {
        return Url::to('/sociomicses/' . $this->id . '/delete-from-favorites');
    }

    public function getUrl()
    {
        return Url::to('/sociomicses/' . $this->id . '-' . $this->slug);
    }

    public function getRelativeDate()
    {
        return Yii::$app->formatter->asDate($this->published_at);
    }

    public function increaseViews()
    {
        $sociomicsViewExists = SociomicsView::find()->where([
            'sociomics_id' => $this->id,
            'session_id' => Yii::$app->session->id
        ])->exists();

        if ($sociomicsViewExists) {
            return true;
        }

        return (new SociomicsView([
            'sociomics_id' => $this->id,
            'session_id' => Yii::$app->session->id,
            'created_at' => date('Y-m-d H:i:s')
        ]))->save();
    }

    public function isFavorite()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return Favorite::find()->where([
            'sociomics_id' => $this->id,
            'user_id' => Yii::$app->user->id
        ])->exists();
    }

    public function isLiked()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return SociomicsLike::find()->where([
            'sociomics_id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'value' => 1
        ])->exists();
    }

    public function isDisliked()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return SociomicsLike::find()->where([
            'sociomics_id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'value' => -1
        ])->exists();
    }

    public function saveUserHistory()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        return (new UserHistory([
            'sociomics_id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'viewed_at' => date('Y-m-d H:i:s')
        ]))->save();
    }
}
