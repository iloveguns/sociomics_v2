<?php

namespace app\models\data;

use Yii;
use yii\helpers\Url;

/**
 * @property int $id
 * @property string $name Имя
 * @property string $role Роль
 * @property string|null $vk ВК
 * @property string|null $facebook Facebook
 * @property string|null $instagram Instagram
 * @property string|null $twitter Twitter
 * @property string|null $email E-mail
 * @property string|null $avatar Аватар
 */
class Author extends \yii\db\ActiveRecord
{
    const ROLE_AUTHOR = 1;
    const ROLE_PAINTER = 2;

    public static function tableName()
    {
        return 'author';
    }

    public function rules()
    {
        return [
            [['name', 'role'], 'required'],
            [['name', 'vk', 'facebook', 'instagram', 'twitter', 'email', 'avatar'], 'string', 'max' => 255],
            ['role', 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'role' => 'Роль',
            'vk' => 'ВК',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'email' => 'E-mail',
            'avatar' => 'Аватар',
        ];
    }

    public static function getRoleList()
    {
        return [
            static::ROLE_AUTHOR => 'Автор',
            static::ROLE_PAINTER => 'Художник'
        ];
    }

    public function getAvatarUrl()
    {
        return $this->avatar ? Url::to('/uploads/' . $this->avatar, true) : '/resources/img/sociomics-detail/author.svg';
    }
}
