<?php

namespace app\models\data;

use Yii;

/**
 * @property int $id
 * @property int $sociomics_id Социомикс
 * @property string $session_id Сессия
 * @property string|null $created_at Время просмотра
 */
class SociomicsView extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'sociomics_view';
    }

    public function rules()
    {
        return [
            [['sociomics_id', 'session_id'], 'required'],
            [['sociomics_id'], 'integer'],
            [['created_at', 'session_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sociomics_id' => 'Социомикс',
            'session_id' => 'Сессия',
            'created_at' => 'Время просмотра',
        ];
    }
}
