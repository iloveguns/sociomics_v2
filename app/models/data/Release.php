<?php

namespace app\models\data;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "release".
 *
 * @property int $id
 * @property string $name Название
 * @property string $date Дата
 * @property string $language Язык
 * @property int $is_published Опубликован
 *
 * @property Sociomics[] $sociomics
 */
class Release extends \yii\db\ActiveRecord
{
    const IS_PUBLISHED_OFF = 0;
    const IS_PUBLISHED_ON = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'release';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'date', 'language'], 'required'],
            [['date'], 'safe'],
            [['is_published'], 'integer'],
            [['is_published'], 'default', 'value' => static::IS_PUBLISHED_OFF],
            [['name', 'language'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date' => 'Дата',
            'language' => 'Язык',
            'is_published' => 'Опубликован',
        ];
    }

    public function beforeSave($insert)
    {
        if (!empty($this->date)) {
            $this->date = date('Y-m-d', strtotime($this->date));
        }

        return parent::beforeSave($insert);
    }

    /**
     * Gets query for [[Sociomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSociomics()
    {
        return $this->hasMany(Sociomics::className(), ['release_id' => 'id']);
    }

    public function getEditUrl()
    {
        return Url::to('/releases/' . $this->id . '/edit');
    }

    public function getDeleteUrl()
    {
        return Url::to('/releases/' . $this->id . '/delete');
    }
}
