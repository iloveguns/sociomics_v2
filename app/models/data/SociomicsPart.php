<?php

namespace app\models\data;

use Yii;
use yii\helpers\Url;

/**
 * @property int $id
 * @property int $sociomics_id Социомикс
 * @property string|null $image Изображение
 * @property string|null $header Заголовок
 * @property string|null $body Текст
 * @property int $order Порядок
 *
 * @property Sociomics $sociomics
 */
class SociomicsPart extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'sociomics_part';
    }

    public function rules()
    {
        return [
            [['sociomics_id'], 'required'],
            [['sociomics_id', 'order'], 'integer'],
            [['body'], 'string'],
            [['image', 'header'], 'string', 'max' => 255],
            [['sociomics_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sociomics::class, 'targetAttribute' => ['sociomics_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sociomics_id' => 'Социомикс',
            'image' => 'Изображение',
            'body' => 'Текст',
            'order' => 'Порядок',
            'header' => 'Заголовок'
        ];
    }

    public function getSociomics()
    {
        return $this->hasOne(Sociomics::class, ['id' => 'sociomics_id']);
    }

    public function getImageUrl()
    {
        return $this->image ? Url::to('/uploads/' . $this->image, true) : '';
    }
}
