<?php

namespace app\assets;

use Yii;
use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/resources';
    public $baseUrl = '@web/resources';
    public $css = [
        'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;800&amp;display=swap',
        'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css',
        'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css',
        'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        'css/app.css',
    ];
    public $js = [
        'https://code.jquery.com/jquery-3.5.1.min.js',
        'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js',
        'https://cdn.tiny.cloud/1/c21cw39l5gi1z5ebnsrs0cscxrpwscn2jzt9zac3lmhi3zy3/tinymce/5/tinymce.min.js',
        'https://yastatic.net/share2/share.js',
        '//ulogin.ru/js/ulogin.js',
        'js/jquery.cookie.js',
        'js/app.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
    ];
}
